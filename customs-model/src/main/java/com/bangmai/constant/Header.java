package com.bangmai.constant;

/**
 * 响应信息必要常量
 * @author PC043
 *
 */
public class Header {
	public final static String STATUS_SUCESS = "0";//成功
	public final static String STATUS_FAILED = "1";//失败
	public final static String STATUS_FATAL_ERROR = "2";//致命错误
	public final static String TASK_RESUME = "resume";//任务恢复
	public final static String TASK_START = "start";//任务开始
	public final static String TASK_PAUSE = "pause";//暂停
	public final static String TASK_DELETE = "delete";//删除
	public final static String TASK_ORDER_SUCCESS="success";//订单缓存成功
	public final static String TASK_ORDER_FAILED="failed";//订单缓存失败
	
}
