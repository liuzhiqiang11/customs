package com.bangmai.model;

import org.springframework.stereotype.Repository;

@Repository
public interface DemoMapper {

	Demo selectDemo(Demo demo);
	void save(Demo demo);
	void update(Demo demo);
}
