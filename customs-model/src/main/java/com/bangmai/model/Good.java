package com.bangmai.model;
/**
 * 商品
 * @author dellpc
 *
 */

public class Good {
	private String skucode;//sku码  商品唯一标识
	private String gcode;//商品货号
	private String hscode;//HS海关编码
	private String ciqgoodsno;//商品备案号
	private String customlistno;//海关商品备案号
	private String copgname;//商品名称
	private String brand;//品牌
	private String spec;//规格
	private String origin;//产地
	private String qtyunit;//计量单位
	private String unitcode;//计量单位代码
	
	private double price;//商品售卖价格
	private String sellwebsite;//销售网址
	private String logistics_code;//物流公司代码 001启邦国际物流，002为南沙国际物流
	private String status;//是否入库（1 入库成功 0还未入库）
	private String notes;//备注
	private String qty;//申报数量
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getSkucode() {
		return skucode;
	}
	public void setSkucode(String skucode) {
		this.skucode = skucode;
	}
	public String getGcode() {
		return gcode;
	}
	public void setGcode(String gcode) {
		this.gcode = gcode;
	}
	public String getHscode() {
		return hscode;
	}
	public void setHscode(String hscode) {
		this.hscode = hscode;
	}
	public String getCiqgoodsno() {
		return ciqgoodsno;
	}
	public void setCiqgoodsno(String ciqgoodsno) {
		this.ciqgoodsno = ciqgoodsno;
	}
	public String getCustomlistno() {
		return customlistno;
	}
	public void setCustomlistno(String customlistno) {
		this.customlistno = customlistno;
	}
	public String getCopgname() {
		return copgname;
	}
	public void setCopgname(String copgname) {
		this.copgname = copgname;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getSpec() {
		return spec;
	}
	public void setSpec(String spec) {
		this.spec = spec;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getQtyunit() {
		return qtyunit;
	}
	public void setQtyunit(String qtyunit) {
		this.qtyunit = qtyunit;
	}
	public String getUnitcode() {
		return unitcode;
	}
	public void setUnitcode(String unitcode) {
		this.unitcode = unitcode;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getSellwebsite() {
		return sellwebsite;
	}
	public void setSellwebsite(String sellwebsite) {
		this.sellwebsite = sellwebsite;
	}
	public String getLogistics_code() {
		return logistics_code;
	}
	public void setLogistics_code(String logistics_code) {
		this.logistics_code = logistics_code;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
	
	
	
	

}
