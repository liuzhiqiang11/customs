package com.bangmai.model;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

/**
 * 数据库操作商品接口
 * @author dellpc
 *
 */
@Component
public interface GoodMapper {
	
	Map<String, Object> selectGood(String skuCode) throws Exception;//根据Sku码来查询商品
	void updateGood(Good good)throws Exception;//更改商品的状态（入库）
	List<Map<String, Object>> selectGoods(List<String> skuCodes)throws Exception;//根据SKU码查询能多条记录
    List<String>  selectProvinceCode()throws Exception;
    List<Map<String, Object>> selectOrderGoods(String order_id) throws Exception;
    void  updateGoodStatus(Good good)throws Exception; //更改商品入库流程状态
}
