package com.bangmai.model;
/**
 * 消息队列推送的商品
 * @author dellpc
 *
 */
public class MqGood {
	private  String goods_sku;//商品SKU
	private  String goods_sort;//商品序号
	private  String goods_price;//商品价格
	private  String goods_num;//商品数量
	private  String goods_total;//商品总价
	private  String orderNo;//订单编号
	public String getGoods_sku() {
		return goods_sku;
	}
	public void setGoods_sku(String goods_sku) {
		this.goods_sku = goods_sku;
	}
	public String getGoods_sort() {
		return goods_sort;
	}
	public void setGoods_sort(String goods_sort) {
		this.goods_sort = goods_sort;
	}
	public String getGoods_price() {
		return goods_price;
	}
	public void setGoods_price(String goods_price) {
		this.goods_price = goods_price;
	}
	public String getGoods_num() {
		return goods_num;
	}
	public void setGoods_num(String goods_num) {
		this.goods_num = goods_num;
	}
	public String getGoods_total() {
		return goods_total;
	}
	public void setGoods_total(String goods_total) {
		this.goods_total = goods_total;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	

	

}
