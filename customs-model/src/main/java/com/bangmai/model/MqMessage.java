package com.bangmai.model;
/**
 * 向网站发出的消息实体
 * @author dellpc
 *
 */
public class MqMessage {
	
	private  String datetime;//发送时间
	private String  orderId;//订单号
	private String logistics_name;//物流名称
	private String logistics_code;//物流单号
	private  String  remarke;//备注
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getLogistics_name() {
		return logistics_name;
	}
	public void setLogistics_name(String logistics_name) {
		this.logistics_name = logistics_name;
	}
	public String getLogistics_code() {
		return logistics_code;
	}
	public void setLogistics_code(String logistics_code) {
		this.logistics_code = logistics_code;
	}
	public String getRemarke() {
		return remarke;
	}
	public void setRemarke(String remarke) {
		this.remarke = remarke;
	}
	
	

}
