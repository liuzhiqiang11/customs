package com.bangmai.model;

import java.util.List;

import com.google.gson.Gson;

/**
 * 消息队列推送的订单
 * 
 * @author dellpc
 *
 */

public class MqOrder {

	private String order_sn;// 订单号
	private String buyer_name;// 订单人姓名
	private String card_type;// 证件类型（01:身份证、02:护照、03:其他）
	private String card_num;// 证件人号码
	private String buyer_phone;// 证件人电话
	private String order_amount;// 商品总额
	private String money_type;// 商品总额币制
	private String shipping_fee;// 运费
	private String shippingMonetary;// 运费币制
	private String order_time;// 订单日期
	private String reciver_address;// 收件人地址
	private String reciver_phone;// 收件人电话
	private String allinpay_trade_no;// 通联支付流水号
	private String pay_way;// 支付渠道（(01：网关支付；02：手机WAP支付；03：线下POS支付；04：手机APP支付；05：预付卡支付；06：便捷付POS支付；07：其他支付渠道；08：新版预付卡支付)）
	private String allinpay_pay_time;// 支付时间
	private String need_list;// 是否需要商品清单（0不需要，1需要）
	private String need_invoice;// 是否需要发票（0不需要，1需要）
	private String invoice_title;// 发票抬头
	private String invoice_content;// 发票内容
	private String save_price;// 保价费
	private  String  tax_fee;//税费
	List<MqGood> goods_info;
	public List<MqGood> getGoods_info() {
		return goods_info;
	}


	public void setGoods_info(List<MqGood> goods_info) {
		this.goods_info = goods_info;
	}


	public String getOrder_sn() {
		return order_sn;
	}


	public void setOrder_sn(String order_sn) {
		this.order_sn = order_sn;
	}


	public String getBuyer_name() {
		return buyer_name;
	}


	public void setBuyer_name(String buyer_name) {
		this.buyer_name = buyer_name;
	}


	public String getCard_type() {
		return card_type;
	}


	public void setCard_type(String card_type) {
		this.card_type = card_type;
	}


	public String getCard_num() {
		return card_num;
	}


	public void setCard_num(String card_num) {
		this.card_num = card_num;
	}


	public String getBuyer_phone() {
		return buyer_phone;
	}


	public void setBuyer_phone(String buyer_phone) {
		this.buyer_phone = buyer_phone;
	}


	public String getOrder_amount() {
		return order_amount;
	}


	public void setOrder_amount(String order_amount) {
		this.order_amount = order_amount;
	}


	public String getMoney_type() {
		return money_type;
	}


	public void setMoney_type(String money_type) {
		this.money_type = money_type;
	}


	public String getShipping_fee() {
		return shipping_fee;
	}


	public void setShipping_fee(String shipping_fee) {
		this.shipping_fee = shipping_fee;
	}


	public String getShippingMonetary() {
		return shippingMonetary;
	}


	public void setShippingMonetary(String shippingMonetary) {
		this.shippingMonetary = shippingMonetary;
	}


	public String getOrder_time() {
		return order_time;
	}


	public void setOrder_time(String order_time) {
		this.order_time = order_time;
	}


	public String getReciver_address() {
		return reciver_address;
	}


	public void setReciver_address(String reciver_address) {
		this.reciver_address = reciver_address;
	}


	public String getReciver_phone() {
		return reciver_phone;
	}


	public void setReciver_phone(String reciver_phone) {
		this.reciver_phone = reciver_phone;
	}


	public String getAllinpay_trade_no() {
		return allinpay_trade_no;
	}


	public void setAllinpay_trade_no(String allinpay_trade_no) {
		this.allinpay_trade_no = allinpay_trade_no;
	}


	public String getPay_way() {
		return pay_way;
	}


	public void setPay_way(String pay_way) {
		this.pay_way = pay_way;
	}


	public String getAllinpay_pay_time() {
		return allinpay_pay_time;
	}


	public void setAllinpay_pay_time(String allinpay_pay_time) {
		this.allinpay_pay_time = allinpay_pay_time;
	}



	public String getNeed_list() {
		return need_list;
	}


	public void setNeed_list(String need_list) {
		this.need_list = need_list;
	}


	public String getNeed_invoice() {
		return need_invoice;
	}


	public void setNeed_invoice(String need_invoice) {
		this.need_invoice = need_invoice;
	}


	public String getInvoice_title() {
		return invoice_title;
	}


	public void setInvoice_title(String invoice_title) {
		this.invoice_title = invoice_title;
	}


	public String getInvoice_content() {
		return invoice_content;
	}


	public void setInvoice_content(String invoice_content) {
		this.invoice_content = invoice_content;
	}


	public String getSave_price() {
		return save_price;
	}


	public void setSave_price(String save_price) {
		this.save_price = save_price;
	}


	public String getTax_fee() {
		return tax_fee;
	}


	public void setTax_fee(String tax_fee) {
		this.tax_fee = tax_fee;
	}


	@Override
	public String toString() {
		return "MqOrder [order_sn=" + order_sn + ", buyer_name=" + buyer_name + ", card_type=" + card_type
				+ ", card_num=" + card_num + ", buyer_phone=" + buyer_phone + ", order_amount=" + order_amount
				+ ", money_type=" + money_type + ", shipping_fee=" + shipping_fee + ", shippingMonetary="
				+ shippingMonetary + ", order_time=" + order_time + ", reciver_address=" + reciver_address
				+ ", reciver_phone=" + reciver_phone + ", allinpay_trade_no=" + allinpay_trade_no + ", pay_way="
				+ pay_way + ", allinpay_pay_time=" + allinpay_pay_time 
				+ ", need_list=" + need_list + ", need_invoice=" + need_invoice + ", invoice_title=" + invoice_title
				+ ", invoice_content=" + invoice_content + ", save_price=" + save_price + ", tax_fee=" + tax_fee + "]";
	}


	/**
	 * json转bean
	 * 
	 * @param json
	 * @return
	 */
	public static MqOrder json2Bean(String json) {
		Gson gson = new Gson();
		MqOrder mqOrder = gson.fromJson(json, MqOrder.class);
		return mqOrder;

	}

}
