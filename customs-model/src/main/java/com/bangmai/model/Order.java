package com.bangmai.model;

import java.util.Date;
import java.util.List;

public class Order {
	
	private String oid;
	private String username;
	private String usercard;
	private String userphone;
	private String useraddress;
	private Float ordergoodstotal;//订单商品总金额
	private String orderdate;
	private Float fare;//运费
	private String tax;//税费
	private String remark;//备注
	private String payTransactionNo;//支付流水号
	private String payChnlID;//支付渠道
	
	private String usercardtype;//证件类型
	private String province;//省
	private  String  city;//市
	private String district;//区
	private String detailaddress;//详细地址
	private String monetarm;//币制
	private String paydatetime;//支付时间
	private String goodsbill;//是否需要商品清单 0不需要 1 需要
	private String invoice;//是否需要发票 0不需要 1需要
	private String invoicetitle;//发票抬头
	private  String invoicecontent;//发票内容
	private  String  insurancefee;//保价费
	private  String paymoney;//实际支付金额
	
	private String goodtype;//商品数量
	
	private String  status;//订单的状态 0 报关流程未完成  1 报关流程已完成
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getGoodtype() {
		return goodtype;
	}
	public void setGoodtype(String goodtype) {
		this.goodtype = goodtype;
	}
	public String getUsercardtype() {
		return usercardtype;
	}
	public void setUsercardtype(String usercardtype) {
		this.usercardtype = usercardtype;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getDetailaddress() {
		return detailaddress;
	}
	public void setDetailaddress(String detailaddress) {
		this.detailaddress = detailaddress;
	}
	public String getMonetarm() {
		return monetarm;
	}
	public void setMonetarm(String monetarm) {
		this.monetarm = monetarm;
	}
	public String getPaydatetime() {
		return paydatetime;
	}
	public void setPaydatetime(String paydatetime) {
		this.paydatetime = paydatetime;
	}
	public String getGoodsbill() {
		return goodsbill;
	}
	public void setGoodsbill(String goodsbill) {
		this.goodsbill = goodsbill;
	}
	public String getInvoice() {
		return invoice;
	}
	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}
	public String getInvoicetitle() {
		return invoicetitle;
	}
	public void setInvoicetitle(String invoicetitle) {
		this.invoicetitle = invoicetitle;
	}
	public String getInvoicecontent() {
		return invoicecontent;
	}
	public void setInvoicecontent(String invoicecontent) {
		this.invoicecontent = invoicecontent;
	}
	public String getInsurancefee() {
		return insurancefee;
	}
	public void setInsurancefee(String insurancefee) {
		this.insurancefee = insurancefee;
	}
	public String getPaymoney() {
		return paymoney;
	}
	public void setPaymoney(String paymoney) {
		this.paymoney = paymoney;
	}
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsercard() {
		return usercard;
	}
	public void setUsercard(String usercard) {
		this.usercard = usercard;
	}
	public String getUserphone() {
		return userphone;
	}
	public void setUserphone(String userphone) {
		this.userphone = userphone;
	}
	public String getUseraddress() {
		return useraddress;
	}
	public void setUseraddress(String useraddress) {
		this.useraddress = useraddress;
	}
	public Float getOrdergoodstotal() {
		return ordergoodstotal;
	}
	public void setOrdergoodstotal(Float ordergoodstotal) {
		this.ordergoodstotal = ordergoodstotal;
	}
	public String getOrderdate() {
		return orderdate;
	}
	public void setOrderdate(String orderdate) {
		this.orderdate = orderdate;
	}
	public Float getFare() {
		return fare;
	}
	public void setFare(Float fare) {
		this.fare = fare;
	}
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getPayTransactionNo() {
		return payTransactionNo;
	}
	public void setPayTransactionNo(String payTransactionNo) {
		this.payTransactionNo = payTransactionNo;
	}
	public String getPayChnlID() {
		return payChnlID;
	}
	public void setPayChnlID(String payChnlID) {
		this.payChnlID = payChnlID;
	}
	@Override
	public String toString() {
		return "Order [oid=" + oid + ", username=" + username + ", usercard=" + usercard + ", userphone=" + userphone
				+ ", useraddress=" + useraddress + ", ordergoodstotal=" + ordergoodstotal + ", orderdate=" + orderdate
				+ ", fare=" + fare + ", tax=" + tax + ", remark=" + remark + ", payTransactionNo=" + payTransactionNo
				+ ", payChnlID=" + payChnlID + "]";
	}
	
	

}
