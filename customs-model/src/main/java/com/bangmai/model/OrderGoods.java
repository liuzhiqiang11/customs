package com.bangmai.model;
/**
 * order_good对应的实体
 * @author dellpc
 *
 */
public class OrderGoods {
	private  String sku;
	private String qty;
	private String decprice;
	private  String dectotal;
	private  String order_id;
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getDecprice() {
		return decprice;
	}
	public void setDecprice(String decprice) {
		this.decprice = decprice;
	}
	public String getdectotal() {
		return dectotal;
	}
	public void setdectotal(String dectotal) {
		this.dectotal = dectotal;
	}
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}

}
