package com.bangmai.model;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

/**
 * 操作order_good表的接口
 * 
 * @author dellpc
 *
 */
@Component
public interface OrderGoodsMapper {

	public void insertOrderGoods(List<OrderGoods> oGoodsList) throws Exception;

	public List<Map<String, Object>> selelctOrderGoods(String order_id) throws Exception;

	public List<Map<String, Object>> selectGoods(List<String> skucodes) throws Exception;

}
