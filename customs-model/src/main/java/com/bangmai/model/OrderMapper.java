package com.bangmai.model;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

/**
 * 操作订单接口
 * 
 * @author dellpc
 *
 */

@Repository
public interface OrderMapper {
	public void insertOrder(Order order) throws Exception;

	public void selectGoods(List<String> skuCodes) throws Exception;

	public List<Map<String, Object>> selectOrders(List<String> order_ids) throws Exception;

	public Map<String, Object> selectOrder(String order_id) throws Exception;
	public  void updateOrder(Order order) throws Exception;
	
	public  void updateOrderStatus(Order order) throws Exception;

}
