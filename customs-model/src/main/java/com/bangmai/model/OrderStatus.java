package com.bangmai.model;
/**
 * 修改报关流程状态实体
 * @author dellpc
 *
 */
public class OrderStatus {
	private String id;
	private String order_id;//订单号
	private String sstatus;//商检
	public String sresult;
	public String sdesc;
	private String hstatus;//海关
	public String hresult;
	public String hdesc;
	private String pstatus;//支付
	public String presult;
	public String pdesc;
	private String wstatus;//物流
	public String wresult;
	public String wdesc;
	public  String flag;//标识
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getSresult() {
		return sresult;
	}
	public void setSresult(String sresult) {
		this.sresult = sresult;
	}
	public String getSdesc() {
		return sdesc;
	}
	public void setSdesc(String sdesc) {
		this.sdesc = sdesc;
	}
	public String getHresult() {
		return hresult;
	}
	public void setHresult(String hresult) {
		this.hresult = hresult;
	}
	public String getHdesc() {
		return hdesc;
	}
	public void setHdesc(String hdesc) {
		this.hdesc = hdesc;
	}
	public String getPresult() {
		return presult;
	}
	public void setPresult(String presult) {
		this.presult = presult;
	}
	public String getPdesc() {
		return pdesc;
	}
	public void setPdesc(String pdesc) {
		this.pdesc = pdesc;
	}
	public String getWresult() {
		return wresult;
	}
	public void setWresult(String wresult) {
		this.wresult = wresult;
	}
	public String getWdesc() {
		return wdesc;
	}
	public void setWdesc(String wdesc) {
		this.wdesc = wdesc;
	}
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	public String getSstatus() {
		return sstatus;
	}
	public void setSstatus(String sstatus) {
		this.sstatus = sstatus;
	}
	public String getHstatus() {
		return hstatus;
	}
	public void setHstatus(String hstatus) {
		this.hstatus = hstatus;
	}
	public String getPstatus() {
		return pstatus;
	}
	public void setPstatus(String pstatus) {
		this.pstatus = pstatus;
	}
	public String getWstatus() {
		return wstatus;
	}
	public void setWstatus(String wstatus) {
		this.wstatus = wstatus;
	}
	@Override
	public String toString() {
		return "OrderStatus [order_id=" + order_id + ", sstatus=" + sstatus + ", sresult=" + sresult + ", sdesc="
				+ sdesc + ", hstatus=" + hstatus + ", hresult=" + hresult + ", hdesc=" + hdesc + ", pstatus=" + pstatus
				+ ", presult=" + presult + ", pdesc=" + pdesc + ", wstatus=" + wstatus + ", wresult=" + wresult
				+ ", wdesc=" + wdesc + ", flag=" + flag + "]";
	}
	
	
	
}
