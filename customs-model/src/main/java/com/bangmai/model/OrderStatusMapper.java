package com.bangmai.model;

import java.util.List;
import java.util.Map;

import org.springframework.orm.jpa.vendor.EclipseLinkJpaDialect;
import org.springframework.stereotype.Component;

/**
 * 修改报关流程状态
 * @author dellpc
 *
 */
@Component
public interface OrderStatusMapper {
	
	
	public void updateStatus(OrderStatus orderStatus) throws Exception;//修改报关 订单流程
	public void updateStatuses(List<OrderStatus> orderStatus) throws Exception;//批量修改报关 订单流程
	public  void  insertStatus(OrderStatus orderStatus) throws Exception;//往订单流程表中添加一条记录
	public String selectFlag(OrderStatus orderStatus) throws Exception;//查询订单流程表中的 flag标识
	public void updateStatusesHg(OrderStatus orderStatus);//批量修改海关相关的字段
	public void updateStatusesSj(OrderStatus orderStatus);//批量修改商检相关的字段
	public void updateStatusesPa(OrderStatus orderStatus);//批量修改通联支付相关的字段
	public void updateStatusesWl(OrderStatus orderStatus);//批量修改物流相关的字段
	public  List<Map<String, Object>> selectOrder(String order_id) throws Exception;
	
	public List<String> selectOrderStatus() throws Exception;
	
	
}
