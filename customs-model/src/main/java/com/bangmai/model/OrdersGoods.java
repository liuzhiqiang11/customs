package com.bangmai.model;

import java.util.List;

public class OrdersGoods {
	
	private  List<String> goodSkus;
	private String order;
	public List<String> getGoodSkus() {
		return goodSkus;
	}
	public void setGoodSkus(List<String> goodSkus) {
		this.goodSkus = goodSkus;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}

}
