package com.bangmai.model;

public class Parameter { 
	private  String  code;
	private String  name;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Parameter [code=" + code + ", name=" + name + "]";
	}

	
}
