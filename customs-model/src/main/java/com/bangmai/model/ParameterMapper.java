package com.bangmai.model;

/**
 * 操作参数码表的接口
 * @author dellpc
 *
 */
public interface ParameterMapper {
	
	public  String  selectParameter(String  name) throws Exception;//通过 name查询 code

}
