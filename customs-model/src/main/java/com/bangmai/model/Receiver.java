package com.bangmai.model;

import java.util.Date;

/**
 * 回执报文解析实体
 * @author dellpc
 *
 */
public class Receiver {
	private String  order_id;
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private String code;
	private String info;
	private Date date;
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	private String status;
	public String getOrder_id() {
		return order_id;
	}
	public void setOrder_id(String order_id) {
		this.order_id = order_id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "Receiver [order_id=" + order_id + ", name=" + name + ", code=" + code + ", info=" + info + ", date="
				+ date + ", status=" + status + "]";
	}
	
	

}
