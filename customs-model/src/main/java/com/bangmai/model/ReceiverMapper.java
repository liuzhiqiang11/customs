package com.bangmai.model;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

/**
 * 数据层接口
 * @author dellpc
 *
 */
@Repository
public interface ReceiverMapper {
	
	void save(List<Receiver> receivers); //向数据库批量插入数据
	void  update(Receiver receiver);//向数据库单条插入数据
	
	
//	Map<String, Object> selectOrder(String order_id);//查询订单表中新增订单
//	Map<String , Object> selectGood(List<String > good_ids);//查询当前订单中的商品
	

}
