package com.bangmai.view;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class CompanyBody {
	private ArrayList xmlBodys;
	@XmlElements({@XmlElement(name="department",type=Department.class)})
	public ArrayList getXmlBodys() {
		return xmlBodys;
	}
	public void setXmlBodys(ArrayList xmlBodys) {
		this.xmlBodys = xmlBodys;
	}
	

}
