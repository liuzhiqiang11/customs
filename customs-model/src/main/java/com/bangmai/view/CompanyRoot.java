package com.bangmai.view;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "root")
public class CompanyRoot {
	private String code;
	private String msg;
	private Object body;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	@XmlElements({@XmlElement(name="departments",type=CompanyBody.class)})
	public Object getBody() {
		return body;
	}

	public void setBody(Object body) {
		this.body = body;
	}

}
