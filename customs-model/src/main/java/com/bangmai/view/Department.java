package com.bangmai.view;

import javax.xml.bind.annotation.XmlElement;

public class Department {
	private String name;
	private Integer count;
	@XmlElement
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@XmlElement
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

}
