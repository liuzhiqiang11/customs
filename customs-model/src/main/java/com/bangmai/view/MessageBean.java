package com.bangmai.view;

/**
 * 
 * @author PC043
 *
 */
public class MessageBean extends PageBean{

	/**
	 * 响应代码，0为正常，1为业务不允许或者业务逻辑出错，2为未知异常
	 */
	private String code;
	private Object body;
	/**
	 * 后台响应具体信息
	 */
	private String msg;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Object getBody() {
		return body;
	}
	public void setBody(Object body) {
		this.body = body;
	}
	 
}
