package com.bangmai.view;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

import com.bangmai.model.Demo;

@XmlRootElement
public class MessageBeanBody {
	private ArrayList xmlBodys;
	@XmlElements({@XmlElement(name="element",type=Demo.class)})
	public ArrayList getXmlBodys() {
		return xmlBodys;
	}
	public void setXmlBodys(ArrayList xmlBodys) {
		this.xmlBodys = xmlBodys;
	}
	public MessageBeanBody() {
		xmlBodys = new ArrayList<>();
	}
}
