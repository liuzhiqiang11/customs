package com.bangmai.view;

/**
 * 分页
 * @author PC043
 *
 */
public class PageBean {

	private Long pageNo;// 当前页
	private Long totalPageSize;// 总数
	private Long pageSize;// 每页显示数量
	private Long pageCount;// 页数

	public Long getPageNo() {
		return (pageNo==null||pageNo<=0)?1:pageNo;
	}

	public void setPageNo(Long pageNo) {
		this.pageNo = pageNo;
	}

	public Long getTotalPageSize() {
		return totalPageSize;
	}

	public void setTotalPageSize(Long totalPageSize) {
		this.totalPageSize = totalPageSize;
	}

	public Long getPageSize() {
		return pageSize;
	}

	public void setPageSize(Long pageSize) {
		this.pageSize = pageSize;
	}

	public Long getPageCount() {
		if(totalPageSize == null || totalPageSize == 0) {
			pageCount = 0l;
		} else {
			if(pageSize == null || pageSize == 0) {
				this.pageCount = 20l;
			} else {
				this.pageCount = totalPageSize/pageSize;
				if(totalPageSize%pageSize!=0) {
					this.pageCount++;
				}
			}
			
		}
		return pageCount;
	}

	public void setPageCount(Long pageCount) {
		this.pageCount = pageCount;
		
	}
}
