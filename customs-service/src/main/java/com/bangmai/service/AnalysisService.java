package com.bangmai.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Component;

import com.bangmai.model.Receiver;

/**
 * 解析回执报文
 * @author dellpc
 *
 */
@Component
public interface AnalysisService {
	
	List<Receiver> analysisCustomXml(List<String> xmList,String ...strings ) throws Exception;//解析海关回执XML文件
	List<Receiver> analysisPaymentXml (List<String> xmList,String ...strings ) throws Exception;//解析支付post方法响应回执报文
	List<Receiver> analysisCommodityinspectionXml(List<String> xmList,String ...strings) throws Exception;//解析商检回执报文
	List<Receiver> analysisLogisticsml(List<String> xmList,String ...strings) throws Exception;//解析南沙物流回执报文（包含入库和出库）
	List<Receiver> analysisQiBang(String xml,String ...strings ) throws Exception;//解析启邦物流回执报文
	List<Receiver> analysisPayMentNotifyXml(String xml,String ...strings)throws Exception;//解析通联支付异步通知回执报文
	
	List<Receiver> analysisStock(List<String> xmls,String ... strings) throws Exception;//解析南沙物流出库成功后回执的物流信息报文
	

}
