package com.bangmai.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.bangmai.beans.Order2CommodityInspection;
import com.bangmai.beans.Order2Customer;
import com.bangmai.beans.Order2Immigration;
import com.bangmai.beans.Order2Payment;
import com.bangmai.beans.Order2QiBang;
import com.bangmai.beans.Order2StockOut;
import com.bangmai.model.MqOrder;
import com.bangmai.model.Order;

/**
 * 从表中查出数据组装成bean
 * @author dellpc
 *
 */
@Component
public interface CreateBeanService {
	
	public  Order2Customer createCustomBean(String  order_id)throws Exception;//海关
	public Order2CommodityInspection createCommodityInspectionBean(String order_id)throws Exception;//商检
	public  Order2Payment createPaymentBean(String order_id)throws Exception;//通联支付
	public List<Order2StockOut> createStockOutBean(List<Map<String , Object>> orders,List<Map<String, Object>> order_goods_map,List<Map<String, Object>> goods_map)throws Exception;//南沙物流出库
	public  List<Order2QiBang> creatQiBangBean(List<Map<String, Object>> qbOrders,List<Map<String, Object>> order_goods_map,List<Map<String, Object>> goods_map)throws Exception;//启邦物流出库
	public Order2Immigration createImmigrationBean(Map<String , Object> map,String qty)throws Exception;//南沙物流入库
}