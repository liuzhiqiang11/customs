package com.bangmai.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

import com.bangmai.model.MqMessage;
import com.bangmai.model.MqOrder;
import com.bangmai.model.Order;
import com.bangmai.model.OrderGoods;
import com.bangmai.model.OrderStatus;
import com.bangmai.model.Receiver;

@Component
public interface CustomsService {
	Logger logger = Logger.getLogger(CustomsService.class.getName());

	/**
	 * 
	 * @param type
	 *            标识 报文的种类 1-海关 ，2-商检，3-通联支付，4-入库，5-出库，6-启邦物流
	 * @throws Exception
	 */

	public void createAndSendPacket(int type, String order_id) throws Exception;// 生成报文

	public void receivePacket(int type) throws Exception;// 加载回执报文

	public void insertOrder(Order order, OrderStatus orderStatus, List<OrderGoods> orderGoods) throws Exception;// 将网站接受的订单加入订单数据库和订单流程表

	public void selectGoods(List<String> skuCodes) throws Exception;// 查询推送的订单中是否还有未备案的商品

	public void updateGood(String skuCode, String logistics_code, String qty) throws Exception;// 入库成功之后更改商品状态

	public void getPaymentNotifyXml(HttpServletRequest request, HttpServletResponse response) throws Exception;// 获取通联支付异步通知报文

	public List<Map<String, Object>> selectOrderStatus(String order_id) throws Exception;// 查询订单状态

	public void insertOrderStatus(OrderStatus orderStatus) throws Exception;// 往订单状态表插入一条记录
	
	public void stockOut() throws Exception;//出库
	public void stockIn(String  types,String order_id) throws Exception;//入库
	
	public  void  getReceiverXml() throws Exception;//定时任务 获取回执报文并解析
	
	public  void  updateOrder(Order order) throws Exception;//更新订单
	
	public  void sendMqmessage2Web(MqMessage mqMessage,String order_id) throws Exception; //报关完成后向网站发送物流信息
}
