package com.bangmai.service;

import java.util.List;
import java.util.Map;

import org.omg.CORBA.PUBLIC_MEMBER;

import com.bangmai.model.Demo;
import com.bangmai.model.Order;

public interface DemoService {

	public void save(Demo demo);
	
	public void update(Demo demo);
	
	public void del(Demo demo);
	
	public  Map<String , Object> select(Integer order_id);
	public Map<String, Object> selectGoods(String skucodes)throws Exception;
}
