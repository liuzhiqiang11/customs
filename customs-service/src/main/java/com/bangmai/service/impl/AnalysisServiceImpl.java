package com.bangmai.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.InputSource;

import com.alibaba.druid.sql.visitor.functions.Now;
import com.bangmai.exception.ServiceException;
import com.bangmai.model.OrderStatus;
import com.bangmai.model.OrderStatusMapper;
import com.bangmai.model.Receiver;
import com.bangmai.service.AnalysisService;

/**
 * 解析XML文件的实现类
 * 
 * @author dellpc
 *
 */
@Component
public class AnalysisServiceImpl implements AnalysisService {
	private Logger logger = Logger.getLogger(AnalysisServiceImpl.class.getName());
	@Autowired
	private OrderStatusMapper orderStatusMapper;

	/**
	 * 海关
	 * 
	 */
	@Override
	public List<Receiver> analysisCustomXml(List<String> xmList, String... strings) throws Exception {
		// TODO Auto-generated method stub
		List<Receiver> receivers = new ArrayList<>();
		if (!xmList.isEmpty()) {
			for (int i = 0; i < xmList.size(); i++) {
				System.out.println(xmList.get(i));
				Receiver receiver = new Receiver();
				OrderStatus oStatus = new OrderStatus();
				SAXBuilder builder = new SAXBuilder();
				StringReader read = new StringReader(xmList.get(i));
				Document document = builder.build(read);
				Element root = document.getRootElement();
				Element element = root.getChild(strings[1]);
//				Element element2 = root.getChild("DocumentNo");
//				Element element3 = root.getChild("ReturnInfo");
//				Element element4 = root.getChild("ReturnCode");
//				if (element2 != null && element3 != null && element4 != null) {
//					receiver.setOrder_id(element2.getValue());
//					receiver.setInfo(element3.getValue());
//					receiver.setCode(element4.getValue());
//				}

				List persons = root.getChildren(strings[0]);
				if (!persons.isEmpty()) {
					for (int j = 0; j < persons.size(); j++) {
						Element person = (Element) persons.get(j);
						List pros = person.getChildren();
						if (!pros.isEmpty()) {
							for (int m = 0; m < pros.size(); m++) {
								Element element_ = (Element) pros.get(m);
								List<Element> info = element_.getChildren();
								if (!info.isEmpty()) {
									for (int n = 0; n < info.size(); n++) {
										Element element5 = info.get(n);
										if (element5.getName().equals("ReturnCode")) {
											receiver.setCode(element5.getValue());
											if (element5.getValue().equals("C01")) {
												receiver.setStatus("1");

											} else {
												receiver.setStatus("0");
												oStatus.setHresult("失败");
											}
										} else if (element5.getName().equals("ReturnInfo")) {
											receiver.setInfo(element5.getValue());
										}else if (element5.getName().equals("DocumentNo")) {
											receiver.setOrder_id(element5.getValue());
											
										}
									}
								}
							}
						} else {
							System.out.println(person.getName() + "--" + person.getValue());
						}

					}
				} else {
					receiver.setStatus("0");
				}
				receiver.setName("海关");
				receiver.setDate(new Date());
				receivers.add(receiver);

				document = null;
				builder = null;
			}
		}
		return receivers;

	}

	/**
	 * 解析通联支付
	 */

	@Override
	public List<Receiver> analysisPaymentXml(List<String> xmList, String... strings) throws Exception {
		// TODO Auto-generated method stub
		List<Receiver> receivers = new ArrayList<>();

		SAXBuilder builder = new SAXBuilder();
		if (!xmList.isEmpty()) {
			for (int i = 0; i < xmList.size(); i++) {
				Receiver receiver = new Receiver();
				StringReader reader = new StringReader(xmList.get(i));
				Document document = builder.build(reader);
				Element root = document.getRootElement();
				Element messagehead = root.getChild("MessageHead");
				Element messageid = messagehead.getChild("MessageId");
				receiver.setOrder_id(messageid.getValue());
				List<Element> elements = root.getChildren(strings[0]);
				for (Element element : elements) {
					if (!element.isRootElement()) {
						Element element2 = element.getChild(strings[1]);
						receiver.setCode(element2.getValue());
						if (element2.getValue() != null && element2.getValue().equals("BZ0001")) {
							receiver.setStatus("1");
						} else {
							receiver.setStatus("0");
						}

					}

				}
				List<Element> elements2 = root.getChildren(strings[2]);
				for (Element element : elements2) {
					if (!element.isRootElement()) {
						List<Element> elements3 = element.getChildren();
						for (Element element2 : elements3) {
							System.out.println(element2.getChild(strings[3]).getName());
							Element element3 = element2.getChild(strings[3]);
							receiver.setInfo(element3.getValue());
						}

					}

				}
				receiver.setDate(new Date());
				receiver.setName("支付");
				receiver.setOrder_id("123456");
				receivers.add(receiver);
				document = null;
				reader.close();

			}
		}

		return receivers;
	}

	/**
	 * 商检
	 */

	@Override
	public List<Receiver> analysisCommodityinspectionXml(List<String> xmList, String... strings) throws Exception {
		// TODO Auto-generated method stub

		List<Receiver> receivers = new ArrayList<>();
		SAXBuilder builder = new SAXBuilder();
		if (!xmList.isEmpty()) {
			for (int i = 0; i < xmList.size(); i++) {
				Receiver receiver = new Receiver();
				StringReader reader = new StringReader(xmList.get(i));
				Document document = builder.build(reader);
				Element root = document.getRootElement();
				Element Declaration = root.getChild(strings[0]);
				if (!Declaration.isRootElement()) {
					List<Element> dElements = Declaration.getChildren();
					for (int j = 0; j < dElements.size(); j++) {
						if (dElements.get(j).getName().equals(strings[2])) {
							receiver.setCode(dElements.get(j).getValue());
							if (dElements.get(j).getValue().equals("10")) {
								receiver.setStatus("1");
							} else {
								receiver.setStatus("0");
							}
						}
						if (dElements.get(j).getName().equals(strings[3])) {
							receiver.setInfo(dElements.get(j).getValue());
						}
						if (dElements.get(j).getName().equals(strings[1])) {
							System.out.println(dElements.get(j).getValue());
							receiver.setOrder_id(dElements.get(j).getValue());

						}

					}

				}
				receiver.setName("商检");
				receiver.setDate(new Date());
				receivers.add(receiver);

			}
		}

		return receivers;
	}

	/**
	 * 物流
	 */

	@Override
	public List<Receiver> analysisLogisticsml(List<String> xmList, String... strings) throws Exception {
		// TODO Auto-generated method stub
		List<Receiver> receivers = new ArrayList<>();
		if (!xmList.isEmpty()) {

			SAXBuilder builder = new SAXBuilder();
			for (int i = 0; i < xmList.size(); i++) {
				Receiver receiver = new Receiver();
				StringReader reader = new StringReader(xmList.get(i));
				Document document = builder.build(reader);
				Element root = document.getRootElement();
				Element EntInsideNo = root.getChild(strings[0]);
				receiver.setOrder_id(EntInsideNo.getValue());
				Element ReturnCode = root.getChild(strings[1]);
				receiver.setCode(ReturnCode.getValue());
				if (ReturnCode.getValue().equals("1")) {
					receiver.setStatus("1");
				} else {
					receiver.setStatus("0");
				}
				Element ReturnInfo = root.getChild(strings[2]);
				receiver.setInfo(ReturnInfo.getValue());
				Element MessageType = root.getChild(strings[3]);
				if (MessageType.getValue().equals("002")) {
					receiver.setName("南沙国际物流入库");
				} else if (MessageType.getValue().equals("003")) {
					receiver.setName("南沙国际物流出库");
				}
				receiver.setDate(new Date());
				receivers.add(receiver);

			}
		} else {
			throw new ServiceException("FTP远程目录中没有文件");
		}
		return receivers;
	}

	@Override
	public List<Receiver> analysisQiBang(String xml, String... strings) throws Exception {
		// TODO Auto-generated method stub
		List<Receiver> receivers = new ArrayList<>();
		SAXBuilder builder = new SAXBuilder(false);
		Document doc = builder.build(new InputSource(new StringReader(xml)));
		Element root = doc.getRootElement(); // 得到根元素
		System.out.println("根节点" + root.getName());
		List<Element> list = root.getChildren(); // 得到元素的集合
		if (!root.isRootElement()) {
			for (int i = 0; i < list.size(); i++) {
				System.out.println(list.get(i).getValue());
				Receiver receiver = new Receiver();

				receiver.setName("启邦物流");
				receiver.setDate(new Date());

			}

		}

		return receivers;
	}

	/**
	 * 解析通联支付异步通知回执的报文
	 */

	@Override
	public List<Receiver> analysisPayMentNotifyXml(String xml, String... strings) throws Exception {
		// TODO Auto-generated method stub
		List<Receiver> receivers = new ArrayList<>();
		Receiver receiver = new Receiver();
		SAXBuilder builder = new SAXBuilder(false);
		Document doc = builder.build(new InputSource(new StringReader(xml)));
		Element root = doc.getRootElement();
		Element orderno = root.getChild(strings[0]);
		receiver.setOrder_id(orderno.getValue());
		Element status = root.getChild(strings[1]);
		receiver.setCode(status.getValue());
		System.out.println(status.getValue());
		if (status.getValue().equals("C01")) {
			receiver.setStatus("1");
		} else {
			receiver.setStatus("0");
		}
		Element msg = root.getChild(strings[2]);
		receiver.setInfo(msg.getValue());
		receiver.setName("通联支付异步通知");
		receiver.setDate(new Date());
		receivers.add(receiver);
		doc = null;
		builder = null;

		return receivers;
	}

	/**
	 * 解析南沙物流出库成功后回执的物流信息报文
	 */

	@Override
	public List<Receiver> analysisStock(List<String> xmls, String... strings) throws Exception {
		// TODO Auto-generated method stub
		if (xmls == null || xmls.isEmpty()) {
			throw new ServiceException("没有获得远程FTP服务器上回执报文!!!");
		}
		List<Receiver> receivers = new ArrayList<>();
		for (String xml : xmls) {
			Receiver receiver = new Receiver();

			receivers.add(receiver);

		}

		return receivers;
	}

}
