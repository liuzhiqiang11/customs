package com.bangmai.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sound.midi.SysexMessage;
import javax.sql.rowset.serial.SerialException;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.bouncycastle.asn1.eac.Flags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bangmai.beans.Order2CommodityInspection;
import com.bangmai.beans.Order2Customer;
import com.bangmai.beans.Order2Immigration;
import com.bangmai.beans.Order2Payment;
import com.bangmai.beans.Order2QiBang;
import com.bangmai.beans.Order2StockOut;
import com.bangmai.beans.OrderBody2Payment;
import com.bangmai.beans.OrderGood2CommodityInspection;
import com.bangmai.beans.OrderGood2Customer;
import com.bangmai.beans.OrderGood2Immigration;
import com.bangmai.beans.OrderGood2QiBang;
import com.bangmai.beans.OrderGood2StockOut;
import com.bangmai.config.Config;
import com.bangmai.exception.ServiceException;
import com.bangmai.model.GoodMapper;
import com.bangmai.model.MqGood;
import com.bangmai.model.MqOrder;
import com.bangmai.model.OrderGoodsMapper;
import com.bangmai.model.OrderMapper;
import com.bangmai.model.OrderStatus;
import com.bangmai.model.OrderStatusMapper;
import com.bangmai.model.ParameterMapper;
import com.bangmai.service.CreateBeanService;
import com.bangmai.util.FileUtil;
import com.bangmai.util.StringUtil;
import com.bangmai.util.TimeUtil;
import com.mysql.fabric.xmlrpc.base.Array;

/**
 * 组装数据
 * 
 * @author dellpc
 *
 */
@Service
public class CreateBeanServicceImpl implements CreateBeanService {
	Logger logger = Logger.getLogger(CreateBeanServicceImpl.class);
	@Autowired
	private GoodMapper goodMapper;
	@Autowired
	private OrderMapper orderMapper;
	@Autowired
	private OrderGoodsMapper orderGoodsMapper;
	@Autowired
	private OrderStatusMapper orderStatusMapper;
	@Autowired
	private ParameterMapper parameterMapper;

	/**
	 * 海关
	 */
	@Override
	@Transactional
	public Order2Customer createCustomBean(String order_id) throws Exception {
		// TODO Auto-generated method stub
		Order2Customer order2Customer = new Order2Customer();
		Map<String, Object> order = orderMapper.selectOrder(order_id);
		List<Map<String, Object>> mqGoods = goodMapper.selectOrderGoods(order_id);
		List<Map<String, Object>> order_goods = orderGoodsMapper.selelctOrderGoods(order_id);
		if (mqGoods == null || mqGoods.isEmpty()) {
			OrderStatus orderStatus = new OrderStatus();
			orderStatus.setOrder_id(order_id);
			orderStatus.setHstatus("W");
			orderStatus.setFlag("0");
			orderStatus.setHdesc("上传失败");
			orderStatus.setHresult("该订单中有商品查询不到信息！！！");
			orderStatusMapper.updateStatusesHg(orderStatus);
			throw new ServiceException("发往海关订单号为：" + order_id + "的订单信息中没有查询到商品信息！！！");
		}
		order2Customer
				.setMessageID(Config.customer_MessageType + TimeUtil.getTimeString1() + FileUtil.get4RandomString());
		order2Customer.setMessageType(Config.customer_MessageType);
		order2Customer.setSenderID(Config.customer_EntRecordNo);
		order2Customer.setSendTime(TimeUtil.long2Date1(System.currentTimeMillis() * 1000));
		order2Customer.setVersion("1.0");

		order2Customer.setOrderId(String.valueOf(order.get("oid")));
		order2Customer.setIEFlag("I");
		order2Customer.setOrderStatus("S");
		order2Customer.setEntRecordNo(Config.customer_EntRecordNo);
		order2Customer.setEntRecordName("广州欧共优电子商务有限公司");
		order2Customer.setOrderName(String.valueOf(order.get("username")));
		order2Customer.setOrderDocType(String.valueOf(order.get("usercardtype")));
		order2Customer.setOrderDocId(String.valueOf(order.get("usercard")));
		order2Customer.setOrderPhone(String.valueOf(order.get("userphone")));
		order2Customer.setOrderGoodTotal(String.valueOf(order.get("ordergoodstotal")));
		order2Customer.setOrderGoodTotalCurr("142");// 人民币
		order2Customer.setFreight(String.valueOf(order.get("fare")));
		order2Customer.setFreightCurr("142");
		order2Customer.setTax(String.valueOf(order.get("tax")));
		order2Customer.setTaxCurr("142");
		order2Customer.setNote("");
		order2Customer.setOrderDate(TimeUtil.long2Date1(Long.valueOf((String.valueOf(order.get("orderdate"))))*1000));
		List<OrderGood2Customer> goods = new ArrayList<>();

		for (int i = 0; i < mqGoods.size(); i++) {
			OrderGood2Customer orderGood2Customer = new OrderGood2Customer();
			orderGood2Customer.setGNo(i + 1 + "");
			orderGood2Customer.setChildOrderNo("");// 可空
			orderGood2Customer.setStoreRecordNo(Config.customer_EntRecordNo);
			orderGood2Customer.setStoreRecordName("帮麦网");
			orderGood2Customer.setCustomsListNO(String.valueOf(mqGoods.get(i).get("customlistno")));// 海关商品备案号
			orderGood2Customer.setDecPrice(String.valueOf(mqGoods.get(i).get("decprice")));
			List<String> units = StringUtil.string2List(String.valueOf(mqGoods.get(i).get("unitcode")));
			if (units==null||units.isEmpty()) {
				throw new ServiceException("计量单位不能为空！！！");
			}
			orderGood2Customer.setUnit(units.get(0));// 计量单位 参考海关
			orderGood2Customer.setGQty(String.valueOf(order_goods.get(i).get("qty")));
			double totalPrice = Double.valueOf(orderGood2Customer.getDecPrice())
					* Integer.valueOf(orderGood2Customer.getGQty());
			orderGood2Customer.setDeclTotal(String.valueOf(totalPrice));
			orderGood2Customer.setNotes("");
			goods.add(orderGood2Customer);
			units = null;
		}
		order2Customer.setGoods(goods);
		return order2Customer;
	}

	/**
	 * 商检
	 */

	@Override
	@Transactional
	public Order2CommodityInspection createCommodityInspectionBean(String order_id) throws Exception {
		// TODO Auto-generated method stub
		Order2CommodityInspection order2CommodityInspection = new Order2CommodityInspection();
		Map<String, Object> order = orderMapper.selectOrder(order_id);
		List<Map<String, Object>> mqGoods = goodMapper.selectOrderGoods(order_id);
		List<Map<String, Object>> order_good = orderGoodsMapper.selelctOrderGoods(order_id);
		if (mqGoods == null || mqGoods.isEmpty()) {
			OrderStatus orderStatus = new OrderStatus();
			orderStatus.setOrder_id(order_id);
			orderStatus.setSstatus("W");
			orderStatus.setFlag("0");
			orderStatus.setSdesc("上传失败");
			orderStatus.setSresult("该订单中有商品查询不到信息！！！");
			orderStatusMapper.updateStatusesSj(orderStatus);
			throw new ServiceException("发往商检订单号为：" + order_id + "的订单信息中没有查询到商品信息！！！");
		}
		// List<String> skuCodes = new ArrayList<>();
		// for (MqGood mqGood : mqGoods) {
		// skuCodes.add(mqGood.getSkuCode());
		// }
		// List<Map<String, Object>> goodList =
		// goodMapper.selectGoods(skuCodes);

		order2CommodityInspection
				.setMessageID(Config.CCIQ_TYPE + TimeUtil.getTimeString1() + FileUtil.get4RandomString());// 订单号
		order2CommodityInspection.setMessageType(Config.CCIQ_TYPE);
		order2CommodityInspection.setSender(Config.CCIQ_SENDER);
		order2CommodityInspection.setReceiver(Config.CCIQ_RECEIVER);
		order2CommodityInspection.setSendTime(TimeUtil.long2Date1(System.currentTimeMillis() * 1000));
		order2CommodityInspection.setFunctionCode("");// 可空
		order2CommodityInspection.setVersion("1.0");
		order2CommodityInspection.setEntInsideNo(String.valueOf(order.get("oid")));
		order2CommodityInspection.setCiqbcode("000069");// 国检组织机构代码
		order2CommodityInspection.setCbeComcode(Config.CCIQ_SENDER);
		order2CommodityInspection.setCbepComcode(Config.CCIQ_SENDER);
		order2CommodityInspection.setOrderStatus("S");
		order2CommodityInspection.setReceiveName(String.valueOf(order.get("username")));
		order2CommodityInspection.setReceiveAddr(String.valueOf(order.get("useraddress")));
		order2CommodityInspection.setReceiveNo(String.valueOf(order.get("usercard")));
		order2CommodityInspection.setReceivePhone(String.valueOf(order.get("userphone")));
		order2CommodityInspection.setFCY(String.valueOf(order.get("ordergoodstotal")));
		order2CommodityInspection.setFcode("CNY");// 商检币种代码 ，人民币
		order2CommodityInspection.setEditccode(Config.CCIQ_SENDER);
		order2CommodityInspection.setDrDate(TimeUtil.long2Date2(System.currentTimeMillis() * 1000));
		List<OrderGood2CommodityInspection> goods = new ArrayList<>();

		for (int i = 0; i < mqGoods.size(); i++) {
			OrderGood2CommodityInspection good = new OrderGood2CommodityInspection();
			good.setEntGoodsNo(i + 1 + "");
			good.setGcode(String.valueOf(mqGoods.get(i).get("gcode")));
			good.setHscode(String.valueOf(mqGoods.get(i).get("hscode")));
			good.setCiqGoodsNo("");// 可空
			good.setCopGName(String.valueOf(mqGoods.get(i).get("copgname")));
			good.setBrand(String.valueOf(mqGoods.get(i).get("brand")));
			good.setSpec(String.valueOf(mqGoods.get(i).get("spec")));
			good.setOrigin(String.valueOf(mqGoods.get(i).get("origin")));
			good.setQty(String.valueOf(order_good.get(i).get("qty")));
			List<String> units=StringUtil.string2List(String.valueOf(mqGoods.get(i).get("unitcode")));
			System.err.println(String.valueOf(mqGoods.get(i).get("unitcode")));
			if (units==null||units.isEmpty()) {
				throw new ServiceException("计量单位不能为空！！！");
			}
			good.setQtyUnit(units.get(1));// 商检计量单位码表
			good.setDecPrice(String.valueOf(mqGoods.get(i).get("price")));
			good.setDecTotal(String.valueOf(Double.valueOf(good.getQty()) * Double.valueOf(good.getDecPrice())));
			good.setSellWebSite("http://www.indoorbuy.com");
			good.setNots("");// 可空
			goods.add(good);
			units=null;
		}
		order2CommodityInspection.setGoods(goods);
		return order2CommodityInspection;
	}

	/**
	 * 通联支付
	 */

	@Override
	@Transactional
	public Order2Payment createPaymentBean(String order_id) throws Exception {
		// TODO Auto-generated method stub
		List<Map<String, Object>> mqGoods = goodMapper.selectOrderGoods(order_id);
		if (mqGoods == null || mqGoods.isEmpty()) {
			OrderStatus orderStatus = new OrderStatus();
			orderStatus.setOrder_id(order_id);
			orderStatus.setPstatus("W");
			orderStatus.setPdesc("上传失败");
			orderStatus.setFlag("0");
			orderStatus.setPresult("该订单中有商品查询不到信息！！！");
			orderStatusMapper.updateStatusesPa(orderStatus);
			throw new ServiceException("发往通联支付订单号为：" + order_id + "的订单信息中没有查询到商品信息！！！");
		}
		Order2Payment order2Payment = new Order2Payment();
		Map<String, Object> order = orderMapper.selectOrder(order_id);
		order2Payment.setMessageCode("VNB3PARTY_PAYVOUCHER");
		order2Payment
				.setMessageID(TimeUtil.long2Date2(System.currentTimeMillis() * 1000) + FileUtil.get4RandomString());
		order2Payment.setSenderID("TEST");
		order2Payment.setSendTime(TimeUtil.long2Date1(System.currentTimeMillis() * 1000));
		order2Payment.setSign("");
		List<OrderBody2Payment> body2Payments = new ArrayList<>();
		OrderBody2Payment body2Payment = new OrderBody2Payment();
		body2Payment.customICP = "TEST000000001";
		body2Payment.ciqType = "01";// 国检组织机构 01-南沙国检
		body2Payment.cbepComCode = Config.CCIQ_SENDER;
		body2Payment.orderNo = String.valueOf(order.get("oid"));
		body2Payment.payTransactionNo = String.valueOf(order.get("payTransactionNo"));
		body2Payment.payChnlID = String.valueOf(order.get("payChnlID"));
		body2Payment.payTime = TimeUtil.diyDate(String.valueOf(order.get("paydatetime")));
		body2Payment.payGoodsAmount = String.valueOf(order.get("ordergoodstotal"));
		body2Payment.payTaxAmount = String.valueOf(order.get("tax"));
		body2Payment.freight = String.valueOf(order.get("fare"));
		body2Payment.payCurrency = "142";// 海关币种代码 人民币
		body2Payment.payerName = String.valueOf(order.get("username"));
		body2Payment.payerDocumentType = String.valueOf(order.get("usercardtype"));
		body2Payment.payerDocumentNumber = String.valueOf(order.get("usercard"));
		body2Payments.add(body2Payment);
		order2Payment.setBodys(body2Payments);
		return order2Payment;
	}

	/**
	 * 出库
	 */
	@Override
	@Transactional
	public List<Order2StockOut> createStockOutBean(List<Map<String, Object>> mqOrder,
			List<Map<String, Object>> order_goods_map, List<Map<String, Object>> goods_map) throws Exception {
		// TODO Auto-generated method stub
		List<Order2StockOut> order2StockOuts = new ArrayList<>();
		for (Map<String, Object> map : mqOrder) {
			Order2StockOut order2StockOut = new Order2StockOut();
			if (order_goods_map == null || order_goods_map.isEmpty()) {
				throw new ServiceException("订单号为：" + map.get("oid") + "的订单没有商品信息！！！");
			}
			// order2StockOut.setMessageID("003" + TimeUtil.getTimeString1() +
			// FileUtil.get4RandomString());
			order2StockOut.setMessageID(
					"003" + TimeUtil.long2Date2(System.currentTimeMillis() * 1000) + FileUtil.get4RandomString());
			order2StockOut.setFunctionCode("0");
			order2StockOut.setMessageType("003");
			order2StockOut.setSenderID("bab");
			order2StockOut.setSendTime(TimeUtil.Date2String(new Date()));
			order2StockOut.setEntInsideNo(String.valueOf(map.get("oid")));// 内部出库编码
																			// ，订单号
			order2StockOut.setInputDate(TimeUtil.Date2String(new Date()));
			order2StockOut.setDrDate(TimeUtil.Date2String(new Date()));
			order2StockOut.setNots("");
			order2StockOut.setIEFlag("I");

			order2StockOut.setGoodInfo(String.valueOf(goods_map.get(0).get("copgname")));
			order2StockOut.setRecipientName(String.valueOf(map.get("username")));
			order2StockOut.setRecipientCountryCode("142");// 收件人所在国家代码，参考海关//
															// ,China
			String provinceCode = parameterMapper.selectParameter(String.valueOf(map.get("province")));
			if (provinceCode == null || provinceCode.isEmpty()) {
				throw new ServiceException("不能查询到对应的省得代码！！！");
			}
			order2StockOut.setRecipientProvincesCode(provinceCode);// 省市代码表，参考国检国别地区码表
			order2StockOut.setRecipientDetailedAddress(String.valueOf(map.get("useraddress")));
			order2StockOut.setRecipientPhone(String.valueOf(map.get("userphone")));
			// 以下几个字段可空
			order2StockOut.setReceiveNo(String.valueOf(map.get("usercard")));
			order2StockOut.setOrderName(String.valueOf(map.get("username")));
			order2StockOut.setOrderDocType(String.valueOf(map.get("usercardtype")));
			order2StockOut.setOrderDocId(String.valueOf(map.get("usercard")));
			//
			order2StockOut.setOrderId(String.valueOf(map.get("oid")));
			order2StockOut.setOrderEntRecordNo(Config.customer_SenderID);// 订单接入企业备案号
			order2StockOut.setExpressCo("");// 可空
			order2StockOut.setCiqBusiMode("10");
			order2StockOut.setCiqEnorderCode(String.valueOf(map.get("oid")));// 国检电商订单号
			order2StockOut.setCiqBcode(Config.CCIQ_BCODE);// 国检组织代码

			order2StockOut.setCiqCbeComcode(Config.CCIQ_SENDER);// 国检跨境电商企业编码
			order2StockOut.setCiqCbepcomcode(Config.CCIQ_SENDER);// 国检跨境电商平台企业编码

			order2StockOut.setExpressProv(String.valueOf(map.get("province")));
			order2StockOut.setExpressCity(String.valueOf(map.get("city")));
			order2StockOut.setExpressArea(String.valueOf(map.get("district")));
			order2StockOut.setExpressRemarks("");// 可空

			order2StockOut.setNeedList("0");
			order2StockOut.setNeedInvoice("0");// 是否需要发票
			order2StockOut.setInvoiceType("");// needinvoice字段是1的时候 必填
			order2StockOut.setInvoiceTitle("");// needinvoice字段是1的时候 必填
			order2StockOut.setInvoiceContent("");// needinvoice字段是1的时候 必填
			// 以下几个字段目前可以空
			order2StockOut.setCiqPayComcode("");
			order2StockOut.setCiqPayno("");// 支付号
			order2StockOut.setCiqPayorName(String.valueOf(map.get("username")));// 付款人
			order2StockOut.setCiqPayFcy(String.valueOf(map.get("paymoney")));// 支付金额
			order2StockOut.setCiqPayFcode("");// 支付币种
			order2StockOut.setCiqPayDate("");// 支付日期
			//
			order2StockOut.setFreight(String.valueOf(map.get("fare")));// 运费
			order2StockOut.setValuationFee("0.00");
			order2StockOut.setExpressNo("");// 可空
			order2StockOut.setNeedWmsOper("1");// 可空,默认1
			order2StockOut.setLogisticsProviders("");// 可空
			order2StockOut.setCustomerCode("bab");
			order2StockOut.setInternetDomainName("www.indoorbuy.com");
			order2StockOut.setRecipientProvincesName(String.valueOf(map.get("province"))
					+ String.valueOf(map.get("city")) + String.valueOf(map.get("district")));
			order2StockOut.setRecipientCityName(String.valueOf(map.get("city")));
			order2StockOut.setCustomAgentCode("44309650PU");
			order2StockOut.setCustomAgentName("广州欧供优电子商务有限公司");
			order2StockOut.setCustomTradeCountry("502");
			order2StockOut.setCustomConsigneeCountryCode("142");
			order2StockOut.setCustomConsigneePhoneNumber(String.valueOf(map.get("userphone")));
			order2StockOut.setCustomTransportTypeCode("2");// 运输方式代码 参考海关运输方式代码
			order2StockOut.setTransportName("船");
			order2StockOut.setCustomPackagingTypeCode("1");// 包装种类代码，参考海关包装代码
			List<OrderGood2StockOut> goods = new ArrayList<>();
			double total_netwt = 0;// 商品总净重
			double total_grosswt = 0;// 商品总毛重
			int total_num = 0;// 商品总件数
			double total_money = 0;// 订单总金额
			for (int i = 0; i < order_goods_map.size(); i++) {
				OrderGood2StockOut good = new OrderGood2StockOut();
				good.setGoodsNo("YLXYZ00099");
				good.setQty(String.valueOf(order_goods_map.get(i).get("qty")));
				good.setDecPrice(String.valueOf(order_goods_map.get(i).get("decprice")));
				good.setDecTotal(String.valueOf(order_goods_map.get(i).get("dectotal")));
				good.setGrossWt(String.valueOf(goods_map.get(i).get("grosswt")));
				good.setNetWt(String.valueOf(goods_map.get(i).get("netwt")));
				good.setNots("");
				good.setGoodsBatchNo("1");
				good.setCustomConversionFactor("10");
				good.setCustomOriginCountryCode("304");// 原产国国家代码，参考海关国家代码good
				total_netwt += Double.valueOf(good.getNetWt()) * Integer.valueOf(good.getQty());
				total_grosswt += Double.valueOf(good.getGrossWt()) * Integer.valueOf(good.getQty());
				total_num += Integer.valueOf(good.getQty());
				total_money += Double.valueOf(good.getDecPrice()) * Integer.valueOf(good.getQty());
				goods.add(good);

			}
			order2StockOut.setGoods(goods);
			order2StockOut.setNetWt(String.valueOf(total_netwt));
			order2StockOut.setGrossWt(String.valueOf(total_grosswt));
			order2StockOut.setNum(String.valueOf(total_num));
			order2StockOut.setCiqFcy(String.valueOf(total_money));// 金额
			order2StockOuts.add(order2StockOut);
		}
		return order2StockOuts;
	}

	/**
	 * 启邦物流
	 */
	@Override
	@Transactional
	public List<Order2QiBang> creatQiBangBean(List<Map<String, Object>> qbOrders,
			List<Map<String, Object>> order_goods_map, List<Map<String, Object>> goods_map) throws Exception {
		// TODO Auto-generated method stub
		List<Order2QiBang> order2QiBangs = new ArrayList<>();
		for (Map<String, Object> map : qbOrders) {
			Order2QiBang order2QiBang = new Order2QiBang();
			double N_kos = 0;// 该订单下的商品毛重
			if (order_goods_map == null || order_goods_map.isEmpty()) {
				throw new ServiceException("订单号：" + map.get("oid") + "的订单没有商品信息！！！");
			}
			order2QiBang.setStore_code("1");// 仓储编码
			order2QiBang.setOrder_code(String.valueOf(map.get("oid")));// 订单编码
			order2QiBang.setOrder_type("201");// 订单类型 参考接口文档
			order2QiBang.setOrder_source("oubeiyou");// 订单来源
			// order2QiBang.setOrder_create_time(TimeUtil.Date2String(new
			// Date()));// 订单创建时间
			order2QiBang.setOrder_create_time(TimeUtil.long2Date1(System.currentTimeMillis() * 1000));
			order2QiBang.setV_ieflag("I");// 进出口标识
			order2QiBang.setV_consignor_code(String.valueOf(map.get("usercard")));// 支付人证件号码
			order2QiBang.setV_card_type(String.valueOf(map.get("usercardtype")));// 证件类型，参考接口文档
			order2QiBang.setV_transport_code("2");// 运输工具，参考接口文档
			order2QiBang.setV_package_typecode("1");// 包装类型 参考接口 bbC模式可空
			order2QiBang.setV_qy_state("502");// 海关国别代码表
			order2QiBang.setV_master_good_name("美孚（Mobil）1号全合成机油 5W-30");

			order2QiBang.setPACKAGE_COUNT(1);// 件数
			order2QiBang.setV_traf_name("飞机");// 运输工具名称
			order2QiBang.setTms_service_code("");// 可空
			order2QiBang.setTms_order_code("");// 可空
			order2QiBang.setPrev_order_code("");// 可空
			order2QiBang.setTotalmailno("");// 可空
			String receiver_info = "^^^" + String.valueOf(map.get("province")) + "^^^" + String.valueOf(map.get("city"))
					+ "^^^" + String.valueOf(map.get("district")) + "^^^" + String.valueOf(map.get("detailaddress"))
					+ "^^^" + String.valueOf(map.get("username")) + "^^^" + String.valueOf(map.get("userphone"))
					+ "^^^NA";

			order2QiBang.setReceiver_info("123456^^^ 四川省^^^成都市^^^金牛区^^^二环路北二段269号^^^刘智强 ^^^13882230271^^^NA");// 收件人地址
			order2QiBang.setSender_info("123456^^^广东省^^^广州市^^^天河区^^^测试地址001^^^中远^^^12345678^^^NA");// 发件人地址
			order2QiBang.setPackage_count(10);// 包裹数量
			order2QiBang.setDelivery_method("");// 发货方式 ，可空
			order2QiBang.setRemark("");// 备注 可空
			order2QiBang.setOrder_ename(String.valueOf(map.get("username")));// 收件人名字
			order2QiBang.setOrder_phone("13882230271");// 收件人电话
			order2QiBang.setOrder_cardno("51081119881105239X");// 收件人证件号码
			order2QiBang.setFreight("10.00");// 运费
			order2QiBang.setTax("6.00");// 税费
			order2QiBang.setInsurance_fee("75.00");// 保价费
			List<OrderGood2QiBang> goods = new ArrayList<>();

			for (int i = 0; i < order_goods_map.size(); i++) {
				OrderGood2QiBang orderGood2QiBang = new OrderGood2QiBang();
				orderGood2QiBang.setV_goods_regist_no("GDO5165602020000072");// 商品备案号，海关
				orderGood2QiBang.setOrder_item_id("1974819");// 订单商品ID
																// ，商家商品的内部货号，可与商品货号item_code相同（BBC、BC必传）
				orderGood2QiBang.setItem_id("1974819");// 商品编码
				orderGood2QiBang.setItem_name(String.valueOf(goods_map.get(i).get("copgname")));// 商品名称
				// orderGood2QiBang.setItem_name("美孚（Mobil）1号全合成机油 5W-30");;
				orderGood2QiBang.setItem_code("1974819");// 商品货号，可与order_item_id相同
				orderGood2QiBang.setInventory_type(1);// 库存类型 默认1 可销售库存
				orderGood2QiBang.setItem_quantity(String.valueOf(order_goods_map.get(i).get("qty")));// 商品数量
				orderGood2QiBang.setItem_price(String.valueOf(goods_map.get(i).get("price")));// 销售价格
				orderGood2QiBang.setItem_version("1");// 商品版本 默认1
				orderGood2QiBang.setAttributes("");// 扩展属性 可空
				orderGood2QiBang.setCus_code("HLKJ");// 商家的代码
				orderGood2QiBang.setSku_code("GDO5165602020000072");// Sku代码
				orderGood2QiBang.setItem_spec("1L/桶 ");// 规格型号

				String grosswet = String.valueOf(goods_map.get(i).get("grosswt"));
				double grosswets = Double.valueOf(grosswet);

				N_kos += Integer.valueOf(orderGood2QiBang.getItem_quantity()) * Double.valueOf(grosswets);
				goods.add(orderGood2QiBang);

			}
			order2QiBang.setN_kos(String.valueOf(N_kos));// 毛重
			order2QiBang.setGoods(goods);
			order2QiBangs.add(order2QiBang);
		}
		return order2QiBangs;
	}

	/**
	 * 入库
	 */
	@Override
	@Transactional
	public Order2Immigration createImmigrationBean(Map<String, Object> map, String qty) throws Exception {
		Order2Immigration order2Immigration = new Order2Immigration();
		order2Immigration.setMessageID("002" + TimeUtil.getTimeString1() + FileUtil.get4RandomString());
		order2Immigration.setFunctionCode("0");
		order2Immigration.setMessageType("002");
		order2Immigration.setSenderID("bab");
		// order2Immigration.setSendTime(TimeUtil.Date2String(new Date()));
		order2Immigration.setSendTime(TimeUtil.long2Date1(System.currentTimeMillis() * 1000));

		order2Immigration.setEntInsideNo(String.valueOf(map.get("skucode")));// 内部出库编码,skucode
		order2Immigration.setInputDate(String.valueOf(map.get("inputdate")));
		order2Immigration.setDrDate(String.valueOf(map.get("dadate")));
		order2Immigration.setCountryCode(String.valueOf(map.get("countrycode")));// 海关起抵过，海关国家代码
		order2Immigration.setCiqbcode(String.valueOf(map.get("ciqbcode")));
		order2Immigration.setCiqBargainnoo(String.valueOf(map.get("ciqbargainnoo")));
		order2Immigration.setCiqShippername(String.valueOf(map.get("ciqshippername")));
		order2Immigration.setCiqConsigneename(String.valueOf(map.get("ciqconsigneename")));
		order2Immigration.setCiqPortload(String.valueOf(map.get("ciqportload")));// 国检起运港,参考国检国外港口码表
		order2Immigration.setCiqPortdis(String.valueOf(map.get("ciqportdis")));// 国检目的港，参考国检国内港口码表
		order2Immigration.setCiqTradecode(String.valueOf(map.get("ciqtradecode")));// 国检贸易国别，参考国检国别地区码表
		order2Immigration.setCiqCountryload(String.valueOf(map.get("ciqcountryload")));// 国检起运国

		order2Immigration.setCiqCbeComcode("1500003460");// 国检跨境电商企业编码
		order2Immigration.setBillNo(String.valueOf(map.get("billno")));
		order2Immigration.setTool(String.valueOf(map.get("tool")));
		order2Immigration.setLogisticsProviders("");// 非必填
		order2Immigration.setCustomerCode("bab");
		order2Immigration.setCarNo("");// 非必填
		order2Immigration.setTransportMode("");// 非必填

		ArrayList<OrderGood2Immigration> goods = new ArrayList<>();
		if (map.isEmpty()) {
			throw new SerialException("发往南沙国际物流入库的信息中没有商品！！！");
		}
		OrderGood2Immigration good2Immigration = new OrderGood2Immigration();
		// good2Immigration.setGoodsNo("YLXYZ00099");
		good2Immigration.setGoodsNo(String.valueOf(map.get("gcode")));
		good2Immigration.setQty(qty);
		good2Immigration.setDecPrice(String.valueOf(map.get("price")));
		good2Immigration
				.setDecTotal(String.valueOf(Double.valueOf(good2Immigration.getDecPrice()) * Integer.valueOf(qty)));
		good2Immigration.setGrossWt(String.valueOf(map.get("grosswt")));
		good2Immigration.setNetWt(String.valueOf(map.get("netwt")));
		good2Immigration.setNots("");
		good2Immigration.setCiqPacktype(String.valueOf(map.get("ciqpacktype")));// 包装方式
																				// 国检包装方式码表
		good2Immigration.setCiqBuyfromcity(String.valueOf(map.get("ciqbuyfromcity")));
		good2Immigration.setCiqGoodsbatchno(String.valueOf(map.get("ciqgoodsbatchno")));
		good2Immigration.setCiqCtSize(String.valueOf(map.get("ciqctsize")));
		good2Immigration.setCiqCtType(String.valueOf(map.get("ciqcttype")));
		good2Immigration.setCtNumber(String.valueOf(map.get("ctnumber")));
		good2Immigration.setBarCode(String.valueOf(map.get("barcode")));
		good2Immigration.setFirstStatutoryUnitCode(String.valueOf(map.get("firststatutoryunitcode")));// 参考海关的计量单位代码
		good2Immigration.setFirstStatutoryQuantity(String.valueOf(map.get("firststatutoryquantity")));// 第一法定数量
		goods.add(good2Immigration);
		order2Immigration.setGoods(goods);
		return order2Immigration;
	}

}
