package com.bangmai.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bangmai.model.Demo;
import com.bangmai.model.DemoMapper;
import com.bangmai.model.GoodMapper;
import com.bangmai.model.Order;
import com.bangmai.model.OrderMapper;
import com.bangmai.service.DemoService;

@Service
public class DemoServiceImpl implements DemoService {
	@Autowired
	private DemoMapper demoMapper;
	@Autowired
	private OrderMapper orderMapper;
	@Autowired
	private GoodMapper goodMapper;
	@Override
	@Transactional
	public void save(Demo demo) {//当有多个表操作时候，出于对是数据一致性和完整性的考虑，需要加上注解@Transactional
		demoMapper.save(demo);
//		demo =null;
		
		
//		if(demo == null) {
//			throw new NullPointerException();
//		}
	}
@Transactional
	@Override
	public void update(Demo demo) {
		// TODO Auto-generated method stub
		demoMapper.update(demo);
	}

	@Override
	@Transactional
	public void del(Demo demo) {
		// TODO Auto-generated method stub

	}
	@Override
	@Transactional
	public Map<String, Object> select(Integer order_id) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Map<String, Object> selectGoods(String skucodes) throws Exception {
		// TODO Auto-generated method stub
		return goodMapper.selectGood(skucodes);
	}



}
