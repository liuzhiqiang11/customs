package com.bangmai.beans;

import java.io.Serializable;
import java.util.List;

/**
 * 发往商检的订单信息实体
 * @author dellpc
 *
 */

public class Order2CommodityInspection  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -8213858490228413814L;

	//报文头部信息
     public String MessageID;
	
	public String MessageType;
	
	public String Receiver;
	
	public String Sender;
	
	public String SendTime;
	public String FunctionCode;
	public String Version;
	
	
	//订单信息
	
    public String EntInsideNo;
	
	public String Ciqbcode;
	
	public String CbeComcode;
	
	public String CbepComcode;
	
	public String OrderStatus;
	
	public String ReceiveName;
	
	public String ReceiveAddr;
	
	public String ReceiveNo;
	
	public String ReceivePhone;
	
	public String FCY;
	
	public String Fcode;
	
	public String Editccode;
	
	public String DrDate;
	
	List<OrderGood2CommodityInspection> goods;

	public List<OrderGood2CommodityInspection> getGoods() {
		return goods;
	}

	public void setGoods(List<OrderGood2CommodityInspection> goods) {
		this.goods = goods;
	}

	public String getMessageID() {
		return MessageID;
	}

	public void setMessageID(String messageID) {
		MessageID = messageID;
	}

	public String getMessageType() {
		return MessageType;
	}

	public void setMessageType(String messageType) {
		MessageType = messageType;
	}

	public String getReceiver() {
		return Receiver;
	}

	public void setReceiver(String receiver) {
		Receiver = receiver;
	}

	public String getSender() {
		return Sender;
	}

	public void setSender(String sender) {
		Sender = sender;
	}

	public String getSendTime() {
		return SendTime;
	}

	public void setSendTime(String sendTime) {
		SendTime = sendTime;
	}

	public String getFunctionCode() {
		return FunctionCode;
	}

	public void setFunctionCode(String functionCode) {
		FunctionCode = functionCode;
	}

	public String getVersion() {
		return Version;
	}

	public void setVersion(String version) {
		Version = version;
	}

	public String getEntInsideNo() {
		return EntInsideNo;
	}

	public void setEntInsideNo(String entInsideNo) {
		EntInsideNo = entInsideNo;
	}

	public String getCiqbcode() {
		return Ciqbcode;
	}

	public void setCiqbcode(String ciqbcode) {
		Ciqbcode = ciqbcode;
	}

	public String getCbeComcode() {
		return CbeComcode;
	}

	public void setCbeComcode(String cbeComcode) {
		CbeComcode = cbeComcode;
	}

	public String getCbepComcode() {
		return CbepComcode;
	}

	public void setCbepComcode(String cbepComcode) {
		CbepComcode = cbepComcode;
	}

	public String getOrderStatus() {
		return OrderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		OrderStatus = orderStatus;
	}

	public String getReceiveName() {
		return ReceiveName;
	}

	public void setReceiveName(String receiveName) {
		ReceiveName = receiveName;
	}

	public String getReceiveAddr() {
		return ReceiveAddr;
	}

	public void setReceiveAddr(String receiveAddr) {
		ReceiveAddr = receiveAddr;
	}

	public String getReceiveNo() {
		return ReceiveNo;
	}

	public void setReceiveNo(String receiveNo) {
		ReceiveNo = receiveNo;
	}

	public String getReceivePhone() {
		return ReceivePhone;
	}

	public void setReceivePhone(String receivePhone) {
		ReceivePhone = receivePhone;
	}

	public String getFCY() {
		return FCY;
	}

	public void setFCY(String fCY) {
		FCY = fCY;
	}

	public String getFcode() {
		return Fcode;
	}

	public void setFcode(String fcode) {
		Fcode = fcode;
	}

	public String getEditccode() {
		return Editccode;
	}

	public void setEditccode(String editccode) {
		Editccode = editccode;
	}

	public String getDrDate() {
		return DrDate;
	}

	public void setDrDate(String drDate) {
		DrDate = drDate;
	}

	
}
