package com.bangmai.beans;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.Gson;
public class Order2Customer  implements Serializable{

	private static final long serialVersionUID = -4627250568296112143L;
	/**
	 * ���͸����صĶ�����Ϣʵ��
	 */

	//Xml����ͷ����
	public String MessageID;//���ı��
	public String MessageType;//��������
	public String SenderID;//������ҵ������
	public String SendTime;//��������
	public String Version;//���İ汾��
	//������ͷ��Ϣ
	public String OrderId;//�������
	public String IEFlag;//�����ڱ�ʶ
	public String OrderStatus;//����״̬
	public String EntRecordNo;//����ƽ̨��ҵ������
	public String EntRecordName;//����ƽ̨��ҵ����
	public String OrderName;//����������
	public String OrderDocType;//������֤������
	public String OrderDocId;//������֤������
	public String OrderPhone;//�����˵绰����
	public String OrderGoodTotal;//������Ʒ�ܶ�
	public String OrderGoodTotalCurr;//������Ʒ�ܶ����
	public String Freight;//�˷�
	public String FreightCurr;//�˷ѱ���
	public String Tax;//˰��
	public String TaxCurr;//�տ����
	public String Note;//��ע
	public String OrderDate;//��������
	
	List<OrderGood2Customer> goods;
	public List<OrderGood2Customer> getGoods() {
		return goods;
	}
	public void setGoods(List<OrderGood2Customer> goods) {
		this.goods = goods;
	}
	public String getMessageID() {
		return MessageID;
	}
	public void setMessageID(String messageID) {
		MessageID = messageID;
	}
	public String getMessageType() {
		return MessageType;
	}
	public void setMessageType(String messageType) {
		MessageType = messageType;
	}
	public String getSenderID() {
		return SenderID;
	}
	public void setSenderID(String senderID) {
		SenderID = senderID;
	}
	public String getSendTime() {
		return SendTime;
	}
	public void setSendTime(String sendTime) {
		SendTime = sendTime;
	}
	public String getVersion() {
		return Version;
	}
	public void setVersion(String version) {
		Version = version;
	}
	public String getOrderId() {
		return OrderId;
	}
	public void setOrderId(String orderId) {
		OrderId = orderId;
	}
	public String getIEFlag() {
		return IEFlag;
	}
	public void setIEFlag(String iEFlag) {
		IEFlag = iEFlag;
	}
	public String getOrderStatus() {
		return OrderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		OrderStatus = orderStatus;
	}
	public String getEntRecordNo() {
		return EntRecordNo;
	}
	public void setEntRecordNo(String entRecordNo) {
		EntRecordNo = entRecordNo;
	}
	public String getEntRecordName() {
		return EntRecordName;
	}
	public void setEntRecordName(String entRecordName) {
		EntRecordName = entRecordName;
	}
	public String getOrderName() {
		return OrderName;
	}
	public void setOrderName(String orderName) {
		OrderName = orderName;
	}
	public String getOrderDocType() {
		return OrderDocType;
	}
	public void setOrderDocType(String orderDocType) {
		OrderDocType = orderDocType;
	}
	public String getOrderDocId() {
		return OrderDocId;
	}
	public void setOrderDocId(String orderDocId) {
		OrderDocId = orderDocId;
	}
	public String getOrderPhone() {
		return OrderPhone;
	}
	public void setOrderPhone(String orderPhone) {
		OrderPhone = orderPhone;
	}
	public String getOrderGoodTotal() {
		return OrderGoodTotal;
	}
	public void setOrderGoodTotal(String orderGoodTotal) {
		OrderGoodTotal = orderGoodTotal;
	}
	public String getOrderGoodTotalCurr() {
		return OrderGoodTotalCurr;
	}
	public void setOrderGoodTotalCurr(String orderGoodTotalCurr) {
		OrderGoodTotalCurr = orderGoodTotalCurr;
	}
	public String getFreight() {
		return Freight;
	}
	public void setFreight(String freight) {
		Freight = freight;
	}
	public String getFreightCurr() {
		return FreightCurr;
	}
	public void setFreightCurr(String freightCurr) {
		FreightCurr = freightCurr;
	}
	public String getTax() {
		return Tax;
	}
	public void setTax(String tax) {
		Tax = tax;
	}
	public String getTaxCurr() {
		return TaxCurr;
	}
	public void setTaxCurr(String taxCurr) {
		TaxCurr = taxCurr;
	}
	public String getNote() {
		return Note;
	}
	public void setNote(String note) {
		Note = note;
	}
	public String getOrderDate() {
		return OrderDate;
	}
	public void setOrderDate(String orderDate) {
		OrderDate = orderDate;
	}
	@Override
	public String toString() {
		return "Order2Customer [MessageID=" + MessageID + ", MessageType=" + MessageType + ", SenderID=" + SenderID
				+ ", SendTime=" + SendTime + ", Version=" + Version + ", OrderId=" + OrderId + ", IEFlag=" + IEFlag
				+ ", OrderStatus=" + OrderStatus + ", EntRecordNo=" + EntRecordNo + ", EntRecordName=" + EntRecordName
				+ ", OrderName=" + OrderName + ", OrderDocType=" + OrderDocType + ", OrderDocId=" + OrderDocId
				+ ", OrderPhone=" + OrderPhone + ", OrderGoodTotal=" + OrderGoodTotal + ", OrderGoodTotalCurr="
				+ OrderGoodTotalCurr + ", Freight=" + Freight + ", FreightCurr=" + FreightCurr + ", Tax=" + Tax
				+ ", TaxCurr=" + TaxCurr + ", Note=" + Note + ", OrderDate=" + OrderDate + ", goods=" + goods + "]";
	}
	
	

}
