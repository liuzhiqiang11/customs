package com.bangmai.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 发送给物流公司的入库报文实体
 * @author dellpc
 *
 */

public class Order2Immigration  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9117154743721394661L;
	public String MessageID;//每份报文唯一的编号，规则：报文类型（3位）+当前系统时间（YYYYMMDDHHMMSS）+4位随机数
	public String FunctionCode;//默认为0
	public String MessageType;//报文类型，默认为002
	public String SenderID;//发送方编码（由我方系统提供）
	public String SendTime;//发送时间，格式yyyy-mm-dd hh:mm:ss
	
	public String EntInsideNo;//内部入库编码
	public String InputDate;//录入日期
	public String DrDate;//进仓日期
	public String Nots;//备注
	public String BillNo;//提单号
	public String CountryCode;//海关起抵国，海关国家代码
	public String Ciqbcode;//检验检疫机构代码
	public String CiqBargainnoo;//合同号
	public String CiqShippername;//发货人名称
	public String CiqConsigneename;//收货人名称
	public String CiqPortload;//国检起运港,国检国外港口码表
	public String CiqPortdis;//国检目的港,国检国内港口码表
	public String CiqTradecode;//国检贸易国别，国检国别地区码表
	public String CiqCountryload;//国检起运国，国检国别地区码表
	public String CiqCbeComcode;//国检跨境电商企业编码
	public String Tool;//运输工具号：可选值：飞机 卡车 火车 船 其他 海运集装箱 陆运集装箱 空运集装箱
	public String LogisticsProviders;//物流商编码，由我方系统提供
	public String CustomerCode;//电商客户编码，由我方系统提供
	
	public String CiqCtType;//商检柜型A: 普通箱B: 超高柜C: 冷藏D: 罐式
	public String CtNumber;//柜号
	public String CarNo;//车牌号 
    public String TransportMode;//运输方式:混装，板装，散装
    
    public  ArrayList<OrderGood2Immigration> goods;
	public ArrayList<OrderGood2Immigration> getGoods() {
		return goods;
	}
	public void setGoods(ArrayList<OrderGood2Immigration> goods) {
		this.goods = goods;
	}
	public String getMessageID() {
		return MessageID;
	}
	public void setMessageID(String messageID) {
		MessageID = messageID;
	}
	public String getFunctionCode() {
		return FunctionCode;
	}
	public void setFunctionCode(String functionCode) {
		FunctionCode = functionCode;
	}
	public String getMessageType() {
		return MessageType;
	}
	public void setMessageType(String messageType) {
		MessageType = messageType;
	}
	public String getSenderID() {
		return SenderID;
	}
	public void setSenderID(String senderID) {
		SenderID = senderID;
	}
	public String getSendTime() {
		return SendTime;
	}
	public void setSendTime(String sendTime) {
		SendTime = sendTime;
	}
	public String getEntInsideNo() {
		return EntInsideNo;
	}
	public void setEntInsideNo(String entInsideNo) {
		EntInsideNo = entInsideNo;
	}
	public String getInputDate() {
		return InputDate;
	}
	public void setInputDate(String inputDate) {
		InputDate = inputDate;
	}
	public String getDrDate() {
		return DrDate;
	}
	public void setDrDate(String drDate) {
		DrDate = drDate;
	}
	public String getNots() {
		return Nots;
	}
	public void setNots(String nots) {
		Nots = nots;
	}
	public String getBillNo() {
		return BillNo;
	}
	public void setBillNo(String billNo) {
		BillNo = billNo;
	}
	public String getCountryCode() {
		return CountryCode;
	}
	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}
	public String getCiqbcode() {
		return Ciqbcode;
	}
	public void setCiqbcode(String ciqbcode) {
		Ciqbcode = ciqbcode;
	}
	public String getCiqBargainnoo() {
		return CiqBargainnoo;
	}
	public void setCiqBargainnoo(String ciqBargainnoo) {
		CiqBargainnoo = ciqBargainnoo;
	}
	public String getCiqShippername() {
		return CiqShippername;
	}
	public void setCiqShippername(String ciqShippername) {
		CiqShippername = ciqShippername;
	}
	public String getCiqConsigneename() {
		return CiqConsigneename;
	}
	public void setCiqConsigneename(String ciqConsigneename) {
		CiqConsigneename = ciqConsigneename;
	}
	public String getCiqPortload() {
		return CiqPortload;
	}
	public void setCiqPortload(String ciqPortload) {
		CiqPortload = ciqPortload;
	}
	public String getCiqPortdis() {
		return CiqPortdis;
	}
	public void setCiqPortdis(String ciqPortdis) {
		CiqPortdis = ciqPortdis;
	}
	public String getCiqTradecode() {
		return CiqTradecode;
	}
	public void setCiqTradecode(String ciqTradecode) {
		CiqTradecode = ciqTradecode;
	}
	public String getCiqCountryload() {
		return CiqCountryload;
	}
	public void setCiqCountryload(String ciqCountryload) {
		CiqCountryload = ciqCountryload;
	}
	public String getCiqCbeComcode() {
		return CiqCbeComcode;
	}
	public void setCiqCbeComcode(String ciqCbeComcode) {
		CiqCbeComcode = ciqCbeComcode;
	}
	public String getTool() {
		return Tool;
	}
	public void setTool(String tool) {
		Tool = tool;
	}
	public String getLogisticsProviders() {
		return LogisticsProviders;
	}
	public void setLogisticsProviders(String logisticsProviders) {
		LogisticsProviders = logisticsProviders;
	}
	public String getCustomerCode() {
		return CustomerCode;
	}
	public void setCustomerCode(String customerCode) {
		CustomerCode = customerCode;
	}
	public String getCiqCtType() {
		return CiqCtType;
	}
	public void setCiqCtType(String ciqCtType) {
		CiqCtType = ciqCtType;
	}
	public String getCtNumber() {
		return CtNumber;
	}
	public void setCtNumber(String ctNumber) {
		CtNumber = ctNumber;
	}
	public String getCarNo() {
		return CarNo;
	}
	public void setCarNo(String carNo) {
		CarNo = carNo;
	}
	public String getTransportMode() {
		return TransportMode;
	}
	public void setTransportMode(String transportMode) {
		TransportMode = transportMode;
	}
	@Override
	public String toString() {
		return "Order2Immigration [MessageID=" + MessageID + ", FunctionCode=" + FunctionCode + ", MessageType="
				+ MessageType + ", SenderID=" + SenderID + ", SendTime=" + SendTime + ", EntInsideNo=" + EntInsideNo
				+ ", InputDate=" + InputDate + ", DrDate=" + DrDate + ", Nots=" + Nots + ", BillNo=" + BillNo
				+ ", CountryCode=" + CountryCode + ", Ciqbcode=" + Ciqbcode + ", CiqBargainnoo=" + CiqBargainnoo
				+ ", CiqShippername=" + CiqShippername + ", CiqConsigneename=" + CiqConsigneename + ", CiqPortload="
				+ CiqPortload + ", CiqPortdis=" + CiqPortdis + ", CiqTradecode=" + CiqTradecode + ", CiqCountryload="
				+ CiqCountryload + ", CiqCbeComcode=" + CiqCbeComcode + ", Tool=" + Tool + ", LogisticsProviders="
				+ LogisticsProviders + ", CustomerCode=" + CustomerCode + ", CiqCtType=" + CiqCtType + ", CtNumber="
				+ CtNumber + ", CarNo=" + CarNo + ", TransportMode=" + TransportMode + ", goods=" + goods + "]";
	}
	
    

}
