package com.bangmai.beans;

import java.util.List;

/**
 * 发送到支付公司的报文信息实体
 * @author dellpc
 *
 */

public class Order2Payment {
	//报文头部属性
		public  String MessageCode;//报文接口编号，默认VNB3PARTY_PAYVOUCHER
		public  String MessageID;//报文编号，唯一标识每份报文，定长18位数字
		public String CommCode;//通讯状态码
		public String BizStatus;//业务状态码
		public  String SenderID;//通讯状态码(请求方不填, 响应方返回)
		public  String SendTime;//时间戳，格式yyyy-mm-dd hh:mm:ss
		public String ReceiptUrl;//海关回执通知地址
		public  String Sign;//数字签名, 摘要算法-MD5，签名内容-报文体(不包含XML声明以及sign域的原始报文体 + 签名密钥)，摘要结果-32位长，16进制大写表示，测试环境密钥
		public List<OrderBody2Payment> bodys;
		public List<OrderBody2Payment> getBodys() {
			return bodys;
		}
		public void setBodys(List<OrderBody2Payment> bodys) {
			this.bodys = bodys;
		}
		public String getMessageCode() {
			return MessageCode;
		}
		public void setMessageCode(String messageCode) {
			MessageCode = messageCode;
		}
		public String getMessageID() {
			return MessageID;
		}
		public void setMessageID(String messageID) {
			MessageID = messageID;
		}
		public String getCommCode() {
			return CommCode;
		}
		public void setCommCode(String commCode) {
			CommCode = commCode;
		}
		public String getBizStatus() {
			return BizStatus;
		}
		public void setBizStatus(String bizStatus) {
			BizStatus = bizStatus;
		}
		public String getSenderID() {
			return SenderID;
		}
		public void setSenderID(String senderID) {
			SenderID = senderID;
		}
		public String getSendTime() {
			return SendTime;
		}
		public void setSendTime(String sendTime) {
			SendTime = sendTime;
		}
		public String getReceiptUrl() {
			return ReceiptUrl;
		}
		public void setReceiptUrl(String receiptUrl) {
			ReceiptUrl = receiptUrl;
		}
		public String getSign() {
			return Sign;
		}
		public void setSign(String sign) {
			Sign = sign;
		}
		
}
