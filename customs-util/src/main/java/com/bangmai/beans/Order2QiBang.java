package com.bangmai.beans;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



/**
 * 发送到启邦物流的订单信息
 * @author dellpc
 *
 */

public class Order2QiBang {
	private String  store_code;//仓储编码，电商平台指定（BBC、BC必传）
	private String  order_code;//订单编码（BBC、BC必传）
	private String  order_type;//操作子类型（BBC、BC必传）001 进口BC接口002 出口BC201销售单进口BBC601采购单进口BBC
	private String  order_source;//订单来源双方约定（BBC、BC必传）
	private String  order_create_time;//格式为yyyy-MM-dd hh:mm:ss（BBC、BC必传）
	private String  v_ieflag;//进出口标识，I:进口；E:出口；（BBC、BC必传）
	private String  v_consignor_code;//支付人代码（BBC、BC如需物流公司报订单必选）进口收货人填写境内消费者证件号码，如：身份证/护照等
	private String  v_card_type;//证件类型（BBC、BC如需物流公司报订单必选）01:身份证、02:护照、03:其他；
	private String  v_transport_code;//出境货物的运输方式是按货物运离我国关境最后一个口岸时的运输方式填报1空运2海运3汽运4铁运(BBC可不传，BC必传，如无由启邦默认)
	private String  v_package_typecode;//海关对进出口货物实际采用的外部包装方式的标识代码，采用1 位数字表示，如：1木箱、2纸箱、3桶装、4散装、5托盘、6包、7油罐车等(BBC可不传，BC必传，如无由启邦默认)
	private String  v_qy_state;//起运国/运抵国（地区）进口为起运国(地区)代码，出口为运抵国(地区)代码(BBC可不传，BC必传，如无由启邦默认)
	private String  v_master_good_name;//主要商品名称（BBC、BC选传）
	private String  n_kos;//毛重（BBC、BC必传）
	private int  PACKAGE_COUNT;//件数
	private String  v_traf_name;//运输工具名称(BBC可不传，BC必传，如无由启邦默认)
	
	private String  tms_service_code;//物流公司编码（BBC、BC选传）销退单会使用物流上门取货的情形则不用
	private String  tms_order_code;//运单号,退货单有可能有运单号（BBC、BC选传）
	private String  prev_order_code;//原ＺＹ订单编码，在退换货时会用到退货入库单 时可能会有（BBC、BC选传）
	private String  totalmailno;//总运单号（BC选传）
	private String  receiver_info;//手机和电话必选其一（BBC、BC必传）收货方信息邮编^^^省^^^市^^^区^^^具体地址^^^收件方名称^^^手机^^^电话
	private String  sender_info;//手机和电话必选其一（BBC、BC必传）邮编^^^省^^^市^^^区^^^具体地址^^^发件方名称^^^手机^^^电话
	
	private int  package_count;//包裹数量（BBC、BC必传）
	private String  delivery_method;//发货方式1启邦BC2启邦BBC3启邦保税直邮
	private String  remark;//备注
	private String  order_ename;//订单人姓名
	private String  order_phone;//订单人电话
	private String  order_cardno;//订单人身份证号码
	private String  freight;//运费
	private String  Insurance_fee;//保价费
	private String  tax;//税费
	private List<OrderGood2QiBang> goods;
	public List<OrderGood2QiBang> getGoods() {
		return goods;
	}
	public void setGoods(List<OrderGood2QiBang> goods) {
		this.goods = goods;
	}
	public int getPackage_count() {
		return package_count;
	}
	public void setPackage_count(int package_count) {
		this.package_count = package_count;
	}
	public String getDelivery_method() {
		return delivery_method;
	}
	public void setDelivery_method(String delivery_method) {
		this.delivery_method = delivery_method;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getOrder_ename() {
		return order_ename;
	}
	public void setOrder_ename(String order_ename) {
		this.order_ename = order_ename;
	}
	public String getOrder_phone() {
		return order_phone;
	}
	public void setOrder_phone(String order_phone) {
		this.order_phone = order_phone;
	}
	public String getOrder_cardno() {
		return order_cardno;
	}
	public void setOrder_cardno(String order_cardno) {
		this.order_cardno = order_cardno;
	}
	public String getFreight() {
		return freight;
	}
	public void setFreight(String freight) {
		this.freight = freight;
	}
	public String getInsurance_fee() {
		return Insurance_fee;
	}
	public void setInsurance_fee(String insurance_fee) {
		Insurance_fee = insurance_fee;
	}
	public String getTax() {
		return tax;
	}
	public void setTax(String tax) {
		this.tax = tax;
	}
	@XmlElement
	public String getStore_code() {
		return store_code;
	}
	public void setStore_code(String store_code) {
		this.store_code = store_code;
	}
	@XmlElement
	public String getOrder_code() {
		return order_code;
	}
	public void setOrder_code(String order_code) {
		this.order_code = order_code;
	}
	@XmlElement
	public String getOrder_type() {
		return order_type;
	}
	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}
	@XmlElement
	public String getOrder_source() {
		return order_source;
	}
	public void setOrder_source(String order_source) {
		this.order_source = order_source;
	}
	@XmlElement
	public String getOrder_create_time() {
		return order_create_time;
	}
	public void setOrder_create_time(String order_create_time) {
		this.order_create_time = order_create_time;
	}
	@XmlElement
	public String getV_ieflag() {
		return v_ieflag;
	}
	public void setV_ieflag(String v_ieflag) {
		this.v_ieflag = v_ieflag;
	}
	@XmlElement
	public String getV_consignor_code() {
		return v_consignor_code;
	}
	public void setV_consignor_code(String v_consignor_code) {
		this.v_consignor_code = v_consignor_code;
	}
	
	@XmlElement
	public String getV_card_type() {
		return v_card_type;
	}
	public void setV_card_type(String v_card_type) {
		this.v_card_type = v_card_type;
	}
	@XmlElement
	public String getV_transport_code() {
		return v_transport_code;
	}
	public void setV_transport_code(String v_transport_code) {
		this.v_transport_code = v_transport_code;
	}
	@XmlElement
	public String getV_package_typecode() {
		return v_package_typecode;
	}
	public void setV_package_typecode(String v_package_typecode) {
		this.v_package_typecode = v_package_typecode;
	}
	@XmlElement
	public String getV_qy_state() {
		return v_qy_state;
	}
	public void setV_qy_state(String v_qy_state) {
		this.v_qy_state = v_qy_state;
	}
	@XmlElement
	public String getV_master_good_name() {
		return v_master_good_name;
	}
	public void setV_master_good_name(String v_master_good_name) {
		this.v_master_good_name = v_master_good_name;
	}
	@XmlElement
	public String getN_kos() {
		return n_kos;
	}
	public void setN_kos(String n_kos) {
		this.n_kos = n_kos;
	}
	@XmlElement
	public int getPACKAGE_COUNT() {
		return PACKAGE_COUNT;
	}
	public void setPACKAGE_COUNT(int pACKAGE_COUNT) {
		PACKAGE_COUNT = pACKAGE_COUNT;
	}
	@XmlElement
	public String getV_traf_name() {
		return v_traf_name;
	}
	public void setV_traf_name(String v_traf_name) {
		this.v_traf_name = v_traf_name;
	}
	@XmlElement
	public String getTms_service_code() {
		return tms_service_code;
	}
	public void setTms_service_code(String tms_service_code) {
		this.tms_service_code = tms_service_code;
	}
	@XmlElement
	public String getTms_order_code() {
		return tms_order_code;
	}
	public void setTms_order_code(String tms_order_code) {
		this.tms_order_code = tms_order_code;
	}
	@XmlElement
	public String getPrev_order_code() {
		return prev_order_code;
	}
	public void setPrev_order_code(String prev_order_code) {
		this.prev_order_code = prev_order_code;
	}
	@XmlElement
	public String getTotalmailno() {
		return totalmailno;
	}
	public void setTotalmailno(String totalmailno) {
		this.totalmailno = totalmailno;
	}
	@XmlElement
	public String getReceiver_info() {
		return receiver_info;
	}
	public void setReceiver_info(String receiver_info) {
		this.receiver_info = receiver_info;
	}
	@XmlElement
	public String getSender_info() {
		return sender_info;
	}
	public void setSender_info(String sender_info) {
		this.sender_info = sender_info;
	}
	
}
