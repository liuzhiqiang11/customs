package com.bangmai.beans;

import java.io.Serializable;
import java.util.List;
/**
 * 物流出库信息报文头及出库头信息
 * @author zhang
 *
 */

public class Order2StockOut implements Serializable{
	public String MessageID;//每份报文唯一的编号，规则：报文类型（3位）+当前系统时间（YYYYMMDDHHMMSS）+4位随机数
	public String FunctionCode;//默认为0
	public String MessageType;//报文类型，默认为003
	public String SenderID;//发送方编码（由我方系统提供）
	public String SendTime;//发送时间，格式yyyy-mm-dd hh:mm:ss
	
	public String EntInsideNo;//内部出库编码
	public String InputDate;//录入日期
	public String DrDate;//出仓日期
	public String Nots;//备注
	public String IEFlag;//E-出口；I-进口；
	public String NetWt;//商品总净重，小数精确到两位；KG
	public String GrossWt;//商品总毛重，小数精确到两位；KG
	public String Num;//商品总件数，>0
	public String GoodInfo;//描述主要商品信息
	public String RecipientName;//收件人姓名
	public String RecipientCountryCode;//收件人所在国家（地区）的代码，海关标准的参数
	public String RecipientProvincesCode;//省市代码表（30位代码）
	public String RecipientDetailedAddress;//收件人地址
	public String RecipientPhone;//收件人电话
	public String ReceiveNo;//收件人证件号码
	public String OrderName;//订单人姓名
	public String OrderDocType;//订单人证件类型，01:身份证、02:护照、03:其他；
	public String OrderDocId;//证件号
	public String OrderId;//订单号
	public String OrderEntRecordNo;//订单接入企业备案号
	public String ExpressCo;//快递公司
	public String ExpressNo;//快递单号，如果有则提供
	public String CiqBusiMode;//国检进口模式，10：备货模式20：集货模式默认10：
	public String CiqEnorderCode;//国检电商订单号
	public String CiqBcode;//国检组织代码
	public String  CiqFcy;//金额
	public String CiqCbeComcode;//国检跨境电商企业编码
	public String CiqCbepcomcode;//国检跨境电商平台企业编码
	public String ExpressProv;//收件人所在省份
	public String ExpressCity;//收件人所在市
	public String ExpressArea;//收件人所在区
	public String ExpressRemarks;//快递备注
	public String NeedList;//是否需要商品清单，0不需要，1需要
	public String NeedInvoice;//是否需要发票，0-不需要，1-需要
	public String InvoiceType;//1-普通发票
	public String InvoiceTitle;//发票抬头
	public String InvoiceContent;//发票内容
	public String CiqPayComcode;//支付平台企业国检备案号（暂填南沙国际国检备案号）
	public String CiqPayno;//支付号
	public String CiqPayorName;//付款人
	public String CiqPayFcy;//支付金额
	public String CiqPayFcode;//支付币种
	public String CiqPayDate;//支付日期YYYY-MM-DD
	public String Freight;//运费
	public String ValuationFee;//保价费
	public String GoodsBatchNo;//批次号当批次号不为空时，系统会根据批次号查找库存进行出库
	public String NeedWmsOper;//是否需要库内操作，默认为10-不需要1-需要
	public String LogisticsProviders;//物流商编码，由我方系统提供
	public String CustomerCode;//电商客户编码，由我方系统提供
	
	
	public String InternetDomainName;//电商平台互联网域名
	public String RecipientProvincesName;//收件省市区名称
	public String RecipientCityName;//收件城市名称
	public String CustomAgentCode;//报关企业海关编码
	public String CustomAgentName;//报关企业名称
	public  String CustomTradeCountry;//起运国，参考海关国家代码表
	public String CustomConsigneeCountryCode;//消费者所在国家（地区）代码, 参考海关国家代码表
	public String CustomConsigneePhoneNumber;//消费者联系电话
	public String getRecipientProvincesName() {
		return RecipientProvincesName;
	}
	public void setRecipientProvincesName(String recipientProvincesName) {
		RecipientProvincesName = recipientProvincesName;
	}
	public String getRecipientCityName() {
		return RecipientCityName;
	}
	public void setRecipientCityName(String recipientCityName) {
		RecipientCityName = recipientCityName;
	}
	public String getCustomTradeCountry() {
		return CustomTradeCountry;
	}
	public void setCustomTradeCountry(String customTradeCountry) {
		CustomTradeCountry = customTradeCountry;
	}
	public String CustomTransportTypeCode;//运输方式代码,参考海关运输方式代码表
	public String TransportName;//运输工具名称
	public String CustomPackagingTypeCode;//包装种类代码，参考海关包装代码
	
	public List<OrderGood2StockOut> goods;
	public List<OrderGood2StockOut> getGoods() {
		return goods;
	}
	public void setGoods(List<OrderGood2StockOut> goods) {
		this.goods = goods;
	}
	public String getMessageID() {
		return MessageID;
	}
	public void setMessageID(String messageID) {
		MessageID = messageID;
	}
	public String getFunctionCode() {
		return FunctionCode;
	}
	public void setFunctionCode(String functionCode) {
		FunctionCode = functionCode;
	}
	public String getMessageType() {
		return MessageType;
	}
	public void setMessageType(String messageType) {
		MessageType = messageType;
	}
	public String getSenderID() {
		return SenderID;
	}
	public void setSenderID(String senderID) {
		SenderID = senderID;
	}
	public String getSendTime() {
		return SendTime;
	}
	public void setSendTime(String sendTime) {
		SendTime = sendTime;
	}
	public String getEntInsideNo() {
		return EntInsideNo;
	}
	public void setEntInsideNo(String entInsideNo) {
		EntInsideNo = entInsideNo;
	}
	public String getInputDate() {
		return InputDate;
	}
	public void setInputDate(String inputDate) {
		InputDate = inputDate;
	}
	public String getDrDate() {
		return DrDate;
	}
	public void setDrDate(String drDate) {
		DrDate = drDate;
	}
	public String getNots() {
		return Nots;
	}
	public void setNots(String nots) {
		Nots = nots;
	}
	public String getIEFlag() {
		return IEFlag;
	}
	public void setIEFlag(String iEFlag) {
		IEFlag = iEFlag;
	}
	public String getNetWt() {
		return NetWt;
	}
	public void setNetWt(String netWt) {
		NetWt = netWt;
	}
	public String getGrossWt() {
		return GrossWt;
	}
	public void setGrossWt(String grossWt) {
		GrossWt = grossWt;
	}
	public String getNum() {
		return Num;
	}
	public void setNum(String num) {
		Num = num;
	}
	public String getGoodInfo() {
		return GoodInfo;
	}
	public void setGoodInfo(String goodInfo) {
		GoodInfo = goodInfo;
	}
	public String getRecipientName() {
		return RecipientName;
	}
	public void setRecipientName(String recipientName) {
		RecipientName = recipientName;
	}
	public String getRecipientCountryCode() {
		return RecipientCountryCode;
	}
	public void setRecipientCountryCode(String recipientCountryCode) {
		RecipientCountryCode = recipientCountryCode;
	}
	public String getRecipientProvincesCode() {
		return RecipientProvincesCode;
	}
	public void setRecipientProvincesCode(String recipientProvincesCode) {
		RecipientProvincesCode = recipientProvincesCode;
	}
	public String getRecipientDetailedAddress() {
		return RecipientDetailedAddress;
	}
	public void setRecipientDetailedAddress(String recipientDetailedAddress) {
		RecipientDetailedAddress = recipientDetailedAddress;
	}
	public String getRecipientPhone() {
		return RecipientPhone;
	}
	public void setRecipientPhone(String recipientPhone) {
		RecipientPhone = recipientPhone;
	}
	public String getReceiveNo() {
		return ReceiveNo;
	}
	public void setReceiveNo(String receiveNo) {
		ReceiveNo = receiveNo;
	}
	public String getOrderName() {
		return OrderName;
	}
	public void setOrderName(String orderName) {
		OrderName = orderName;
	}
	public String getOrderDocType() {
		return OrderDocType;
	}
	public void setOrderDocType(String orderDocType) {
		OrderDocType = orderDocType;
	}
	public String getOrderDocId() {
		return OrderDocId;
	}
	public void setOrderDocId(String orderDocId) {
		OrderDocId = orderDocId;
	}
	public String getOrderId() {
		return OrderId;
	}
	public void setOrderId(String orderId) {
		OrderId = orderId;
	}
	public String getOrderEntRecordNo() {
		return OrderEntRecordNo;
	}
	public void setOrderEntRecordNo(String orderEntRecordNo) {
		OrderEntRecordNo = orderEntRecordNo;
	}
	public String getExpressCo() {
		return ExpressCo;
	}
	public void setExpressCo(String expressCo) {
		ExpressCo = expressCo;
	}
	public String getExpressNo() {
		return ExpressNo;
	}
	public void setExpressNo(String expressNo) {
		ExpressNo = expressNo;
	}
	public String getCiqBusiMode() {
		return CiqBusiMode;
	}
	public void setCiqBusiMode(String ciqBusiMode) {
		CiqBusiMode = ciqBusiMode;
	}
	public String getCiqEnorderCode() {
		return CiqEnorderCode;
	}
	public void setCiqEnorderCode(String ciqEnorderCode) {
		CiqEnorderCode = ciqEnorderCode;
	}
	public String getCiqBcode() {
		return CiqBcode;
	}
	public void setCiqBcode(String ciqBcode) {
		CiqBcode = ciqBcode;
	}
	public String getCiqFcy() {
		return CiqFcy;
	}
	public void setCiqFcy(String ciqFcy) {
		CiqFcy = ciqFcy;
	}
	public String getCiqCbeComcode() {
		return CiqCbeComcode;
	}
	public void setCiqCbeComcode(String ciqCbeComcode) {
		CiqCbeComcode = ciqCbeComcode;
	}
	public String getCiqCbepcomcode() {
		return CiqCbepcomcode;
	}
	public void setCiqCbepcomcode(String ciqCbepcomcode) {
		CiqCbepcomcode = ciqCbepcomcode;
	}
	public String getExpressProv() {
		return ExpressProv;
	}
	public void setExpressProv(String expressProv) {
		ExpressProv = expressProv;
	}
	public String getExpressCity() {
		return ExpressCity;
	}
	public void setExpressCity(String expressCity) {
		ExpressCity = expressCity;
	}
	public String getExpressArea() {
		return ExpressArea;
	}
	public void setExpressArea(String expressArea) {
		ExpressArea = expressArea;
	}
	public String getExpressRemarks() {
		return ExpressRemarks;
	}
	public void setExpressRemarks(String expressRemarks) {
		ExpressRemarks = expressRemarks;
	}
	public String getNeedList() {
		return NeedList;
	}
	public void setNeedList(String needList) {
		NeedList = needList;
	}
	public String getNeedInvoice() {
		return NeedInvoice;
	}
	public void setNeedInvoice(String needInvoice) {
		NeedInvoice = needInvoice;
	}
	public String getInvoiceType() {
		return InvoiceType;
	}
	public void setInvoiceType(String invoiceType) {
		InvoiceType = invoiceType;
	}
	public String getInvoiceTitle() {
		return InvoiceTitle;
	}
	public void setInvoiceTitle(String invoiceTitle) {
		InvoiceTitle = invoiceTitle;
	}
	public String getInvoiceContent() {
		return InvoiceContent;
	}
	public void setInvoiceContent(String invoiceContent) {
		InvoiceContent = invoiceContent;
	}
	public String getCiqPayComcode() {
		return CiqPayComcode;
	}
	public void setCiqPayComcode(String ciqPayComcode) {
		CiqPayComcode = ciqPayComcode;
	}
	public String getCiqPayno() {
		return CiqPayno;
	}
	public void setCiqPayno(String ciqPayno) {
		CiqPayno = ciqPayno;
	}
	public String getCiqPayorName() {
		return CiqPayorName;
	}
	public void setCiqPayorName(String ciqPayorName) {
		CiqPayorName = ciqPayorName;
	}
	public String getCiqPayFcy() {
		return CiqPayFcy;
	}
	public void setCiqPayFcy(String ciqPayFcy) {
		CiqPayFcy = ciqPayFcy;
	}
	public String getCiqPayFcode() {
		return CiqPayFcode;
	}
	public void setCiqPayFcode(String ciqPayFcode) {
		CiqPayFcode = ciqPayFcode;
	}
	public String getCiqPayDate() {
		return CiqPayDate;
	}
	public void setCiqPayDate(String ciqPayDate) {
		CiqPayDate = ciqPayDate;
	}
	public String getFreight() {
		return Freight;
	}
	public void setFreight(String freight) {
		Freight = freight;
	}
	public String getValuationFee() {
		return ValuationFee;
	}
	public void setValuationFee(String valuationFee) {
		ValuationFee = valuationFee;
	}
	public String getGoodsBatchNo() {
		return GoodsBatchNo;
	}
	public void setGoodsBatchNo(String goodsBatchNo) {
		GoodsBatchNo = goodsBatchNo;
	}
	public String getNeedWmsOper() {
		return NeedWmsOper;
	}
	public void setNeedWmsOper(String needWmsOper) {
		NeedWmsOper = needWmsOper;
	}
	public String getLogisticsProviders() {
		return LogisticsProviders;
	}
	public void setLogisticsProviders(String logisticsProviders) {
		LogisticsProviders = logisticsProviders;
	}
	public String getCustomerCode() {
		return CustomerCode;
	}
	public void setCustomerCode(String customerCode) {
		CustomerCode = customerCode;
	}
	public String getInternetDomainName() {
		return InternetDomainName;
	}
	public void setInternetDomainName(String internetDomainName) {
		InternetDomainName = internetDomainName;
	}
	public String getCustomConsigneePhoneNumber() {
		return CustomConsigneePhoneNumber;
	}
	public void setCustomConsigneePhoneNumber(String customConsigneePhoneNumber) {
		CustomConsigneePhoneNumber = customConsigneePhoneNumber;
	}
	public String getCustomConsigneeCountryCode() {
		return CustomConsigneeCountryCode;
	}
	public void setCustomConsigneeCountryCode(String customConsigneeCountryCode) {
		CustomConsigneeCountryCode = customConsigneeCountryCode;
	}
	public String getCustomTransportTypeCode() {
		return CustomTransportTypeCode;
	}
	public void setCustomTransportTypeCode(String customTransportTypeCode) {
		CustomTransportTypeCode = customTransportTypeCode;
	}
	public String getTransportName() {
		return TransportName;
	}
	public void setTransportName(String transportName) {
		TransportName = transportName;
	}
	public String getCustomPackagingTypeCode() {
		return CustomPackagingTypeCode;
	}
	public void setCustomPackagingTypeCode(String customPackagingTypeCode) {
		CustomPackagingTypeCode = customPackagingTypeCode;
	}
	public String getCustomAgentCode() {
		return CustomAgentCode;
	}
	public void setCustomAgentCode(String customAgentCode) {
		CustomAgentCode = customAgentCode;
	}
	public String getCustomAgentName() {
		return CustomAgentName;
	}
	public void setCustomAgentName(String customAgentName) {
		CustomAgentName = customAgentName;
	}
	
	

}
