package com.bangmai.beans;
/**
 * 发送到支付公司的MessageBody相关信息实体
 * @author dellpc
 *
 */
public class OrderBody2Payment {
	
	//报文主体属性
			public String customICP;//电商对海关接入企业备案号
			public String ciqType;//国检组织机构类型，南沙国检-01，机场国检-02
			public String cbepComCode;//跨境电商平台企业在智检平台做企业备案，审核通过后的企业备案号。
			public String orderNo;//电商平台订单编号
			public String payTransactionNo;//通联支付流水号
			public String payChnlID;//支付渠道(01：网关支付；02：手机WAP支付；03：线下POS支付；04：手机APP支付；05：预付卡支付；06：便捷付POS支付；07：其他支付渠道；08：新版预付卡支付) 
			public String payTime;//通联支付流水支付时间，格式yyyy-mm-dd hh:mm:ss
			public String payGoodsAmount;//支付货款，单位(元, 保留2位小数)
			public String payTaxAmount;//支付税款，单位(元, 保留2位小数)
			public String freight;//支付运费，单位(元, 保留2位小数)
			public String payCurrency;//(货币类型)海关标准参数库,见附录一
			public String payerName;//电商订单注册人姓名
			public String payerDocumentType;//注册人证件类型(01:身份证、 02:护照、03:其他)
			public String payerDocumentNumber;//注册人证件号码
			public String getCustomICP() {
				return customICP;
			}
			public void setCustomICP(String customICP) {
				this.customICP = customICP;
			}
			public String getCiqType() {
				return ciqType;
			}
			public void setCiqType(String ciqType) {
				this.ciqType = ciqType;
			}
			public String getCbepComCode() {
				return cbepComCode;
			}
			public void setCbepComCode(String cbepComCode) {
				this.cbepComCode = cbepComCode;
			}
			public String getOrderNo() {
				return orderNo;
			}
			public void setOrderNo(String orderNo) {
				this.orderNo = orderNo;
			}
			public String getPayTransactionNo() {
				return payTransactionNo;
			}
			public void setPayTransactionNo(String payTransactionNo) {
				this.payTransactionNo = payTransactionNo;
			}
			public String getPayChnlID() {
				return payChnlID;
			}
			public void setPayChnlID(String payChnlID) {
				this.payChnlID = payChnlID;
			}
			public String getPayTime() {
				return payTime;
			}
			public void setPayTime(String payTime) {
				this.payTime = payTime;
			}
			public String getPayGoodsAmount() {
				return payGoodsAmount;
			}
			public void setPayGoodsAmount(String payGoodsAmount) {
				this.payGoodsAmount = payGoodsAmount;
			}
			public String getPayTaxAmount() {
				return payTaxAmount;
			}
			public void setPayTaxAmount(String payTaxAmount) {
				this.payTaxAmount = payTaxAmount;
			}
			public String getFreight() {
				return freight;
			}
			public void setFreight(String freight) {
				this.freight = freight;
			}
			public String getPayCurrency() {
				return payCurrency;
			}
			public void setPayCurrency(String payCurrency) {
				this.payCurrency = payCurrency;
			}
			public String getPayerName() {
				return payerName;
			}
			public void setPayerName(String payerName) {
				this.payerName = payerName;
			}
			public String getPayerDocumentType() {
				return payerDocumentType;
			}
			public void setPayerDocumentType(String payerDocumentType) {
				this.payerDocumentType = payerDocumentType;
			}
			public String getPayerDocumentNumber() {
				return payerDocumentNumber;
			}
			public void setPayerDocumentNumber(String payerDocumentNumber) {
				this.payerDocumentNumber = payerDocumentNumber;
			}
			

}
