package com.bangmai.beans;
/**
 * 发往商检的订单中商品
 * @author dellpc
 *
 */
public class OrderGood2CommodityInspection {
	public String EntGoodsNo;
    public String Gcode;
	
	public String Hscode;
	
	public String CiqGoodsNo;
	
	public String CopGName;
	
	public String Brand;
	
	public String Spec;
	
	public String Origin;
	
	public String  Qty;
	
	public String QtyUnit;
	
	public String DecPrice;
	
	public String DecTotal;
	
	public String SellWebSite;
	
	public String Nots;
	
	public String EntInsideNo;

	public String getGcode() {
		return Gcode;
	}

	public void setGcode(String gcode) {
		Gcode = gcode;
	}

	public String getHscode() {
		return Hscode;
	}

	public void setHscode(String hscode) {
		Hscode = hscode;
	}

	public String getCiqGoodsNo() {
		return CiqGoodsNo;
	}

	public void setCiqGoodsNo(String ciqGoodsNo) {
		CiqGoodsNo = ciqGoodsNo;
	}

	public String getCopGName() {
		return CopGName;
	}

	public void setCopGName(String copGName) {
		CopGName = copGName;
	}

	public String getBrand() {
		return Brand;
	}

	public void setBrand(String brand) {
		Brand = brand;
	}

	public String getSpec() {
		return Spec;
	}

	public void setSpec(String spec) {
		Spec = spec;
	}

	public String getOrigin() {
		return Origin;
	}

	public void setOrigin(String origin) {
		Origin = origin;
	}

	public String getQty() {
		return Qty;
	}

	public void setQty(String qty) {
		Qty = qty;
	}

	public String getQtyUnit() {
		return QtyUnit;
	}

	public void setQtyUnit(String qtyUnit) {
		QtyUnit = qtyUnit;
	}

	public String getDecPrice() {
		return DecPrice;
	}

	public void setDecPrice(String decPrice) {
		DecPrice = decPrice;
	}

	public String getDecTotal() {
		return DecTotal;
	}

	public void setDecTotal(String decTotal) {
		DecTotal = decTotal;
	}

	public String getSellWebSite() {
		return SellWebSite;
	}

	public void setSellWebSite(String sellWebSite) {
		SellWebSite = sellWebSite;
	}

	public String getNots() {
		return Nots;
	}

	public void setNots(String nots) {
		Nots = nots;
	}

	public String getEntInsideNo() {
		return EntInsideNo;
	}

	public void setEntInsideNo(String entInsideNo) {
		EntInsideNo = entInsideNo;
	}

	public String getEntGoodsNo() {
		return EntGoodsNo;
	}

	public void setEntGoodsNo(String entGoodsNo) {
		EntGoodsNo = entGoodsNo;
	}
	
	

}
