package com.bangmai.beans;

import java.io.Serializable;

/**
 * 入库商品信息实体
 * @author dellpc
 *
 */

public class OrderGood2Immigration  implements Serializable{
	public String GoodsNo;//商品编码
	public String Qty;//申报单位总数量
	public String DecPrice;//申报单价, 单位：RMB ，4位小数
	public String DecTotal;//申报总价, 单位：RMB ，二位小数
	public String GrossWt;//毛重，KG
	public String NetWt;//净重,KG
	public String Nots;//备注
	public String CiqPacktype;//包装方式，国检包装方式码表
	public String CiqBuyfromcity;//采购城市
	public String CiqGoodsbatchno;//商品批次号
	public String CiqCtSize;//商检柜尺寸10:10 20:20 25:25 40:40 45:45
	public String CiqCtType;//商检柜型A: 普通箱B: 超高C: 冷藏D: 罐式
	public String CtNumber;//柜号
	public String BarCode;//条码
	public String FirstStatutoryUnitCode;//第一法定计量单位代码，参考海关码表。
    public String FirstStatutoryQuantity;//第一法定数量
	public String getGoodsNo() {
		return GoodsNo;
	}
	public void setGoodsNo(String goodsNo) {
		GoodsNo = goodsNo;
	}
	public String getQty() {
		return Qty;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public String getDecPrice() {
		return DecPrice;
	}
	public void setDecPrice(String decPrice) {
		DecPrice = decPrice;
	}
	public String getDecTotal() {
		return DecTotal;
	}
	public void setDecTotal(String decTotal) {
		DecTotal = decTotal;
	}
	public String getGrossWt() {
		return GrossWt;
	}
	public void setGrossWt(String grossWt) {
		GrossWt = grossWt;
	}
	public String getNetWt() {
		return NetWt;
	}
	public void setNetWt(String netWt) {
		NetWt = netWt;
	}
	public String getNots() {
		return Nots;
	}
	public void setNots(String nots) {
		Nots = nots;
	}
	public String getCiqPacktype() {
		return CiqPacktype;
	}
	public void setCiqPacktype(String ciqPacktype) {
		CiqPacktype = ciqPacktype;
	}
	public String getCiqBuyfromcity() {
		return CiqBuyfromcity;
	}
	public void setCiqBuyfromcity(String ciqBuyfromcity) {
		CiqBuyfromcity = ciqBuyfromcity;
	}
	public String getCiqGoodsbatchno() {
		return CiqGoodsbatchno;
	}
	public void setCiqGoodsbatchno(String ciqGoodsbatchno) {
		CiqGoodsbatchno = ciqGoodsbatchno;
	}
	public String getCiqCtSize() {
		return CiqCtSize;
	}
	public void setCiqCtSize(String ciqCtSize) {
		CiqCtSize = ciqCtSize;
	}
	public String getCiqCtType() {
		return CiqCtType;
	}
	public void setCiqCtType(String ciqCtType) {
		CiqCtType = ciqCtType;
	}
	public String getCtNumber() {
		return CtNumber;
	}
	public void setCtNumber(String ctNumber) {
		CtNumber = ctNumber;
	}
	public String getBarCode() {
		return BarCode;
	}
	public void setBarCode(String barCode) {
		BarCode = barCode;
	}
	public String getFirstStatutoryUnitCode() {
		return FirstStatutoryUnitCode;
	}
	public void setFirstStatutoryUnitCode(String firstStatutoryUnitCode) {
		FirstStatutoryUnitCode = firstStatutoryUnitCode;
	}
	public String getFirstStatutoryQuantity() {
		return FirstStatutoryQuantity;
	}
	public void setFirstStatutoryQuantity(String firstStatutoryQuantity) {
		FirstStatutoryQuantity = firstStatutoryQuantity;
	}
    

}
