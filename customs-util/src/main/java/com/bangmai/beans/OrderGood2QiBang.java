package com.bangmai.beans;

import javax.xml.bind.annotation.XmlElement;

/**
 * 发送到启邦的商品信息
 * @author dellpc
 *
 */

public class OrderGood2QiBang {
	private String order_item_id;
	private String v_goods_regist_no;
	private String item_id;
	private String item_name;
	private String item_code;
	private int inventory_type;
	private String item_quantity;
	private String item_price;
	private String item_version;
	private String attributes;
	private String cus_code;
	private String sku_code;
	private  String item_spec;
	@XmlElement
	public String getOrder_item_id() {
		return order_item_id;
	}
	public void setOrder_item_id(String order_item_id) {
		this.order_item_id = order_item_id;
	}
	@XmlElement
	public String getV_goods_regist_no() {
		return v_goods_regist_no;
	}
	public void setV_goods_regist_no(String v_goods_regist_no) {
		this.v_goods_regist_no = v_goods_regist_no;
	}
	@XmlElement
	public String getItem_id() {
		return item_id;
	}
	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}
	@XmlElement
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	@XmlElement
	public String getItem_code() {
		return item_code;
	}
	public void setItem_code(String item_code) {
		this.item_code = item_code;
	}
	@XmlElement
	public int getInventory_type() {
		return inventory_type;
	}
	public void setInventory_type(int inventory_type) {
		this.inventory_type = inventory_type;
	}
	@XmlElement
	public String getItem_quantity() {
		return item_quantity;
	}
	public void setItem_quantity(String item_quantity) {
		this.item_quantity = item_quantity;
	}
	@XmlElement
	public String getItem_price() {
		return item_price;
	}
	public void setItem_price(String item_price) {
		this.item_price = item_price;
	}
	@XmlElement
	public String getItem_version() {
		return item_version;
	}
	public void setItem_version(String item_version) {
		this.item_version = item_version;
	}
	@XmlElement
	public String getAttributes() {
		return attributes;
	}
	public void setAttributes(String attributes) {
		this.attributes = attributes;
	}
	@XmlElement
	public String getCus_code() {
		return cus_code;
	}
	public void setCus_code(String cus_code) {
		this.cus_code = cus_code;
	}
	@XmlElement
	public String getSku_code() {
		return sku_code;
	}
	public void setSku_code(String sku_code) {
		this.sku_code = sku_code;
	}
	@XmlElement
	public String getItem_spec() {
		return item_spec;
	}
	public void setItem_spec(String item_spec) {
		this.item_spec = item_spec;
	}
	
	
	

}
