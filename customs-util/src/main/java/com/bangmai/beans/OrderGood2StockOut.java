package com.bangmai.beans;

import java.io.Serializable;
/**
 * 物流出库商品实体
 * @author zhang
 *
 */

public class OrderGood2StockOut  implements Serializable{
	public String GoodsNo;//商品编码
	public String Qty;//商品申报数量，>0
	public String DecPrice;//申报单价，4位小数；Rmb的单价；
	public String DecTotal;//申报总价，二位小数
	public String GrossWt;//毛重
	public String NetWt;//净重
	public String Nots;//备注
	public String GoodsBatchNo;//批次号
	public String CustomConversionFactor;//换算系数
	public String CustomOriginCountryCode ;//原产国代码 参考海关国家代码表+
	public String getGoodsNo() {
		return GoodsNo;
	}
	public void setGoodsNo(String goodsNo) {
		GoodsNo = goodsNo;
	}
	public String getQty() {
		return Qty;
	}
	public void setQty(String qty) {
		Qty = qty;
	}
	public String getDecPrice() {
		return DecPrice;
	}
	public void setDecPrice(String decPrice) {
		DecPrice = decPrice;
	}
	public String getDecTotal() {
		return DecTotal;
	}
	public void setDecTotal(String decTotal) {
		DecTotal = decTotal;
	}
	public String getGrossWt() {
		return GrossWt;
	}
	public void setGrossWt(String grossWt) {
		GrossWt = grossWt;
	}
	public String getNetWt() {
		return NetWt;
	}
	public void setNetWt(String netWt) {
		NetWt = netWt;
	}
	public String getNots() {
		return Nots;
	}
	public void setNots(String nots) {
		Nots = nots;
	}
	public String getGoodsBatchNo() {
		return GoodsBatchNo;
	}
	public void setGoodsBatchNo(String goodsBatchNo) {
		GoodsBatchNo = goodsBatchNo;
	}
	public String getCustomConversionFactor() {
		return CustomConversionFactor;
	}
	public void setCustomConversionFactor(String customConversionFactor) {
		CustomConversionFactor = customConversionFactor;
	}
	public String getCustomOriginCountryCode() {
		return CustomOriginCountryCode;
	}
	public void setCustomOriginCountryCode(String customOriginCountryCode) {
		CustomOriginCountryCode = customOriginCountryCode;
	}
	

}
