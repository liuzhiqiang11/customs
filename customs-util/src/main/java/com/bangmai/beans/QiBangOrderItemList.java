package com.bangmai.beans;

import java.util.ArrayList;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class QiBangOrderItemList {
	private ArrayList list;

	@XmlElements({@XmlElement(name = "order_item", type = OrderGood2QiBang.class)})
	public ArrayList getList() {
		return list;
	}

	public void setList(ArrayList list) {
		this.list = list;
	}

}
