package com.bangmai.config;
/**
 * 报文相关属性配置
 * @author dellpc
 *
 */

public class Config {
	//海关
	public static String customer_SenderID="44309650PU";//接入企业备案号
	public static String customer_EntRecordNo="IE151211647456";//电商平台
	public static String customer_MessageType="880020";//报文类型
	
	public  static String FTP_CUSTOM_HOST="210.21.48.7";
	public  static String FTP_CUSTOM_PORT="2312";
	public  static String FTP_CUSTOM_USERNAME="GZOGY";
	public  static String FTP_CUSTOM_PASSWORD="GZOGY";
	public  static String FTP_CUSTOM_UPLOAD_FILEPATH="/UPLOAD";//上传报文所在的远程文件夹
	public  static String FTP_CUSTOM_DOWNLOAD_FILEPATH="/DOWNLOAD";//下载报文所在的远程文件夹
	public static String FTP_CUSTOM_DOWNLOAD_LOCALFILEPATH="D:/custom/custom/out";//存放回执报文的本地路径
//	public static String FTP_CUSTOM_DOWNLOAD_LOCALFILEPATH="/usr/java/custom/custom";//linux路径
//	public static String  CUSTOM_IN="D:/custom/custom/in";//存放发送报文的本地路径
	//商检
	public static String CCIQ_TYPE="661101";//报文类型
	public static String CCIQ_SENDER = "1500003460";  //企业在智检平台的企业备案号
	public static String CCIQ_RECEIVER = "1500003460";
	public static String CCIQ_BCODE="000069";//商检组织代码
	public static String FTP_CCIQ_IP="121.33.205.117";
    public static String FTP_CCIQ_PORT="2221";
    public static String FTP_CCIQ_USERNAME="gzogydzsw";
    public static String FTP_CCIQ_PASSWORD="gzodzytg5tsdf";
	public static String FTP_CCIQ_UPLOADPATH =  "/4200.IMPBA.SWBEBTRADE.REPORT"+"/in";
	public static String FTP_CCIQ_DOWNLOADPATH = "/4200.IMPBA.SWBEBTRADE.REPORT"+ "/out";
	public static String FTP_CCIQ_DOWNLOAD_LOCALFILEPATH="D:/custom/commodityinspection/out";//存放回执报文的本地路径
	public static String  CCIQ_IN="D:/custom/commodityinspection/in";//存放发送报文的本地路径
//	public  static String CCIQ_IN="/usr/java/custom/commodityinspection";

	
	//支付(以下数据为测试环境数据)
	public  static String PAY_CODE="TEST";//电商标识
	public static String PAY_KEY="3D7D15BA3FEF491F9096AC4CB7BB0893";//签名的key
	public  static String PAY_URL="http://121.8.157.114:17090/vnbcustoms/CustomsServlet";//测试地址
	public  static String PAY_LOCALFILRPATH="D:/custom/payment/out";
	public static String PAY_IN="D:/custom/payment/in";//存放发送报文的本地路径
	
	//物流出库(以下是物流出库的测试环境数据)
	public static String STOCKOUT_IP="183.62.44.67";
	public static String STOCKOUT_PORT="21";
	public static String STOCKOUT_USERNAME="NS_TEST";
	public static String STOCKOUT_PASSWORD="ns@test";
	public  static String STOCKOUT_UPLOAD_FILEPATH="/NS_TEST/IN";//上传报文所在的远程文件夹
	public  static String STOCKOUT_DOWNLOAD_FILEPATH="/NS_TEST/OUT";//下载报文所在的远程文件夹
	
	public  static String WL_IMMIGRATION_LOCALFILEPATH_OUT="D:/custom/immigration/out";//入库回执报文保存到本地路径
//	public  static String WL_IMMIGRATION_LOCALFILEPATH_OUT="/usr/java/custom/stockout";
//	public  static String WL_IMMIGRATION_LOCALFILEPATH_IN="D:/custom/immigration/in";//存放发送报文的本地路径
	
	public  static String WL_STOCKOUT_LOCALFILEPATH_OUT="D:/custom/stockout/out";//出库回执报文保存到本地路径
//	public  static String WL_STOCKOUT_LOCALFILEPATH_IN="D:/custom/stockout/in";//存放发送报文的本地路径
	//启邦（  测试数据）
	public  static String QB_KEY="774s3dc97s032de";
	public  static String QB_URL="http://183.62.139.94:22223/msd_gyWMS/gateway.action";
//	public  static String QB_URL="http://202.96.170.100:22223/msd_gyWMS/gateway";
	public  static  String QB_IN="D:/custom/qibang/in";
	public  static  String QB_OUT="D:/custom/qibang/out";
	
	
	
	
	

}
