package com.bangmai.custom;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement(name="Declaration")
public class Declaration {
	private EOrder eOrder;
	private EOrderGoods eOrderGoods;
@XmlElement
	public EOrder geteOrder() {
		return eOrder;
	}

	public void seteOrder(EOrder eOrder) {
		this.eOrder = eOrder;
	}
@XmlElement
	public EOrderGoods geteOrderGoods() {
		return eOrderGoods;
	}

	public void seteOrderGoods(EOrderGoods eOrderGoods) {
		this.eOrderGoods = eOrderGoods;
	}
	
	

}
