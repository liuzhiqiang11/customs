package com.bangmai.custom;

import javax.xml.bind.annotation.XmlElement;

public class EOrder {
	//订单表头信息
		public String OrderId;//订单编号
		public String IEFlag;//进出口标识
		public String OrderStatus;//订单状态
		public String EntRecordNo;//电商平台企业备案号
		public String EntRecordName;//电商平台企业名称
		public String OrderName;//订单人姓名
		public String OrderDocType;//订单人证件类型
		public String OrderDocId;//订单人证件号码
		public String OrderPhone;//订单人电话号码
		public String OrderGoodTotal;//订单商品总额
		public String OrderGoodTotalCurr;//订单商品总额币制
		public String Freight;//运费
		public String FreightCurr;//运费币制
		public String Tax;//税款
		public String TaxCurr;//收款币制
		public String Note;//备注
		public String OrderDate;//订单日期
		@XmlElement
		public String getOrderId() {
			return OrderId;
		}
		public void setOrderId(String orderId) {
			OrderId = orderId;
		}
		@XmlElement
		public String getIEFlag() {
			return IEFlag;
		}
		public void setIEFlag(String iEFlag) {
			IEFlag = iEFlag;
		}
		@XmlElement
		public String getOrderStatus() {
			return OrderStatus;
		}
		public void setOrderStatus(String orderStatus) {
			OrderStatus = orderStatus;
		}
		@XmlElement
		public String getEntRecordNo() {
			return EntRecordNo;
		}
		public void setEntRecordNo(String entRecordNo) {
			EntRecordNo = entRecordNo;
		}
		@XmlElement
		public String getEntRecordName() {
			return EntRecordName;
		}
		public void setEntRecordName(String entRecordName) {
			EntRecordName = entRecordName;
		}
		@XmlElement
		public String getOrderName() {
			return OrderName;
		}
		public void setOrderName(String orderName) {
			OrderName = orderName;
		}
		@XmlElement
		public String getOrderDocType() {
			return OrderDocType;
		}
		public void setOrderDocType(String orderDocType) {
			OrderDocType = orderDocType;
		}
		@XmlElement
		public String getOrderDocId() {
			return OrderDocId;
		}
		public void setOrderDocId(String orderDocId) {
			OrderDocId = orderDocId;
		}
		@XmlElement
		public String getOrderPhone() {
			return OrderPhone;
		}
		public void setOrderPhone(String orderPhone) {
			OrderPhone = orderPhone;
		}
		@XmlElement
		public String getOrderGoodTotal() {
			return OrderGoodTotal;
		}
		public void setOrderGoodTotal(String orderGoodTotal) {
			OrderGoodTotal = orderGoodTotal;
		}
		@XmlElement
		public String getOrderGoodTotalCurr() {
			return OrderGoodTotalCurr;
		}
		public void setOrderGoodTotalCurr(String orderGoodTotalCurr) {
			OrderGoodTotalCurr = orderGoodTotalCurr;
		}
		@XmlElement
		public String getFreight() {
			return Freight;
		}
		public void setFreight(String freight) {
			Freight = freight;
		}
		@XmlElement
		public String getFreightCurr() {
			return FreightCurr;
		}
		public void setFreightCurr(String freightCurr) {
			FreightCurr = freightCurr;
		}
		@XmlElement
		public String getTax() {
			return Tax;
		}
		public void setTax(String tax) {
			Tax = tax;
		}
		@XmlElement
		public String getTaxCurr() {
			return TaxCurr;
		}
		public void setTaxCurr(String taxCurr) {
			TaxCurr = taxCurr;
		}
		@XmlElement
		public String getNote() {
			return Note;
		}
		public void setNote(String note) {
			Note = note;
		}
		@XmlElement
		public String getOrderDate() {
			return OrderDate;
		}
		public void setOrderDate(String orderDate) {
			OrderDate = orderDate;
		}
		
	

}
