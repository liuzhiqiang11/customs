package com.bangmai.custom;

import javax.xml.bind.annotation.XmlElement;

public class EOrderGood {
	public String GNo;// 商品序号
	public String ChildOrderNo;// 子订单编号
	public String StoreRecordNo;// 电商商户企业备案号
	public String StoreRecordName;// 电商商户企业名称
	public String CustomsListNO;// 商品海关备案号
	public String DecPrice;// 商品单价
	public String Unit;// 计量单位
	public String GQty;// 商品数量
	public String DeclTotal;// 商品总价
	public String Notes;// 备注
	@XmlElement
	public String getGNo() {
		return GNo;
	}
	public void setGNo(String gNo) {
		GNo = gNo;
	}
	@XmlElement
	public String getChildOrderNo() {
		return ChildOrderNo;
	}
	public void setChildOrderNo(String childOrderNo) {
		ChildOrderNo = childOrderNo;
	}
	@XmlElement
	public String getStoreRecordNo() {
		return StoreRecordNo;
	}
	public void setStoreRecordNo(String storeRecordNo) {
		StoreRecordNo = storeRecordNo;
	}
	@XmlElement
	public String getStoreRecordName() {
		return StoreRecordName;
	}
	public void setStoreRecordName(String storeRecordName) {
		StoreRecordName = storeRecordName;
	}
	@XmlElement
	public String getCustomsListNO() {
		return CustomsListNO;
	}
	public void setCustomsListNO(String customsListNO) {
		CustomsListNO = customsListNO;
	}
	@XmlElement
	public String getDecPrice() {
		return DecPrice;
	}
	public void setDecPrice(String decPrice) {
		DecPrice = decPrice;
	}
	@XmlElement
	public String getUnit() {
		return Unit;
	}
	public void setUnit(String unit) {
		Unit = unit;
	}
	@XmlElement
	public String getGQty() {
		return GQty;
	}
	public void setGQty(String gQty) {
		GQty = gQty;
	}
	@XmlElement
	public String getDeclTotal() {
		return DeclTotal;
	}
	public void setDeclTotal(String declTotal) {
		DeclTotal = declTotal;
	}
	@XmlElement
	public String getNotes() {
		return Notes;
	}
	public void setNotes(String notes) {
		Notes = notes;
	}
	

}
