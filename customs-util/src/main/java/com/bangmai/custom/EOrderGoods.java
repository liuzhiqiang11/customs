package com.bangmai.custom;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class EOrderGoods {
	private ArrayList eorderGoods;
	@XmlElements({@XmlElement(name = "eOrderGood", type = EOrderGood.class) })
	public ArrayList getEorderGoods() {
		return eorderGoods;
	}
	public void setEorderGoods(ArrayList eorderGoods) {
		this.eorderGoods = eorderGoods;
	}

}
