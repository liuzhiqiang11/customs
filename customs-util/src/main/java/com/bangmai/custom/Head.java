package com.bangmai.custom;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement(name="Head")
public class Head {
	//Xml报文头参数
		public String MessageID;//报文编号
		public String MessageType;//报文类型
		public String SenderID;//接入企业备案号
		public String SendTime;//报文日期
		public String Version;//报文版本号
		@XmlElement
		public String getMessageID() {
			return MessageID;
		}
		public void setMessageID(String messageID) {
			MessageID = messageID;
		}
		@XmlElement
		public String getMessageType() {
			return MessageType;
		}
		public void setMessageType(String messageType) {
			MessageType = messageType;
		}
		@XmlElement
		public String getSenderID() {
			return SenderID;
		}
		public void setSenderID(String senderID) {
			SenderID = senderID;
		}
		@XmlElement
		public String getSendTime() {
			return SendTime;
		}
		public void setSendTime(String sendTime) {
			SendTime = sendTime;
		}
		@XmlElement
		public String getVersion() {
			return Version;
		}
		public void setVersion(String version) {
			Version = version;
		}
		
	


}
