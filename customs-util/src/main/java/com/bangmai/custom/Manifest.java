package com.bangmai.custom;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement(name="Manifest")
public class Manifest {
	private Head head;
	private Declaration declaration;
@XmlElement
	public Head getHead() {
		return head;
	}

	public void setHead(Head head) {
		this.head = head;
	}
@XmlElement
	public Declaration getDeclaration() {
		return declaration;
	}

	public void setDeclaration(Declaration declaration) {
		this.declaration = declaration;
	}
	

}
