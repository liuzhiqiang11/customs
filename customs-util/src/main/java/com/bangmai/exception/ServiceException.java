package com.bangmai.exception;

/**
 * 自定义异常
 * @author PC043
 *
 */
public class ServiceException extends Exception{
	public ServiceException(){
		super();
	}
	
	
	public ServiceException(String msg){
		super(msg);
	}
}
