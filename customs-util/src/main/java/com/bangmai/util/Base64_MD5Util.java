package com.bangmai.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

import com.bangmai.beans.QiBangOrderItemList;


public class Base64_MD5Util {
	private static Decoder decoder = Base64.getDecoder();
	private static Encoder encoder=Base64.getEncoder();
	/**
	 * 加密
	 * @param s
	 * @return
	 */
	public static String base64encode(String s){
		try{
			byte[] encodeStr = encoder.encode(s.getBytes());
			
			return new String(encodeStr,"utf-8");
		}catch(Exception e){
			return s;
		}
	}
	/**
	 * 解密 
	 * @param s
	 * @return
	 */
	
	public static String base64decode(String s){
		try{
			String decodeStr = new String(decoder.decode(s));
						
			return decodeStr;
		}catch(Exception e){
			return s;
		}
	}
	/**
	 * MD5加密
	 * @param s
	 * @return
	 */
	public static String md5encode(String s){
		byte[] digestedValue = null;
		try{
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(s.getBytes());;
			digestedValue = md.digest();
		}catch(Exception e){
			e.printStackTrace();
		}
		try {
			return new String(digestedValue,"utf-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 启邦  MD5加密
	 * @param plainText
	 * @param charset
	 * @return
	 * @throws Exception
	 */
	public static String md5(String plainText,String charset) throws Exception {
    	MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(plainText.getBytes(charset));
        byte b[] = md.digest();
        int i;
        StringBuffer buf = new StringBuffer("");
        for (int offset = 0; offset < b.length; offset++) {
           i = b[offset];
           if (i < 0)
              i += 256;
           if (i < 16)
              buf.append("0");
           buf.append(Integer.toHexString(i));
        }
        return buf.toString();
    }


}
