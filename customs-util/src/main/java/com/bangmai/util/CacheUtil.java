package com.bangmai.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.bangmai.exception.ServiceException;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * 缓存API
 * 
 * @author PC043
 *
 */
@Service
public class CacheUtil {// 和官方命令一样的格式，方便查询，具体请查询http://doc.redisfans.com/，没有写完，需要时再补充
	private JedisPool cachePool;

	public JedisPool getCachePool() {
		return cachePool;
	}

	public void setCachePool(JedisPool cachePool) {
		this.cachePool = cachePool;
	}

	Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 将值 value 关联到 key ，并将 key 的生存时间设为 seconds (以秒为单位)。 如果 key 已经存在， SETEX
	 * 命令将覆写旧值。
	 * 
	 * @param key
	 *            key
	 * @param expireSecond
	 *            超时时间,以秒为单位
	 * @param value
	 *            值
	 * @throws ServiceException
	 */

	public void setEx(String key, int expireSecond, String value) throws ServiceException {// 这里似乎可以AOP
		Jedis jedis = null;
		try {
			jedis = cachePool.getResource();
			jedis.setex(key, expireSecond, value);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new ServiceException("缓存出错，请查看错误日志");
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
	}

	/**
	 * 返回 key 所关联的字符串值。
	 * 
	 * 如果 key 不存在那么返回特殊值 nil 。
	 * 
	 * 假如 key 储存的值不是字符串类型，返回一个错误，因为 GET 只能用于处理字符串值。
	 * 
	 * @param key
	 * @return
	 * @throws ServiceException
	 */
	public boolean exist(String key) throws ServiceException {
		Jedis jedis = null;
		boolean result = false;
		try {
			jedis = cachePool.getResource();
			result = jedis.exists(key);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new ServiceException("缓存出错，请查看错误日志");
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
		return result;

	}

	/**
	 * 删除指定KEY
	 * 
	 * @param key
	 * @return 不等于0删除成功
	 * @throws ServiceException
	 */
	public Long del(String key) throws ServiceException {
		Jedis jedis = null;
		Long result = 0l;
		try {
			jedis = cachePool.getResource();
			result = jedis.del(key);
		} catch (Exception ex) {
			logger.error(ex.getMessage());
			throw new ServiceException("缓存出错，请查看错误日志");
		} finally {
			if (jedis != null) {
				jedis.close();
			}
		}
		return result;
	}
}
