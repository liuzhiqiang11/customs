package com.bangmai.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


public class ExecutorPool {
	private static final Logger logger = LogManager.getLogger(ExecutorPool.class);

	private ExecutorPool() {
		logger.info("init threadPool....");
	}

	private  ExecutorService instance = null;
	public void init() {
		instance = Executors.newCachedThreadPool();
	}
	
//	public void shutdown(){
//		instance.shutdown();
//	}
	
	public  ExecutorService getInstance() {
		return instance;
	}
	
}
