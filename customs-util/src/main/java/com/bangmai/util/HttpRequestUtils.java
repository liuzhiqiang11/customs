package com.bangmai.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.annotation.PostConstruct;

import org.springframework.web.client.HttpClientErrorException;



/**
 * HHTPЭ����ز���������
 * @author dellpc
 *
 */
public class HttpRequestUtils {
	
	
	/**
	 * http协议 post
	 * @param param
	 * @param posturl
	 * @return
	 */

	public static byte[] post(byte[] param, String posturl,String localfilepath) {
		try {
			URL url = new URL(posturl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			connection.setUseCaches(false);
			connection.setInstanceFollowRedirects(true);
			connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
			connection.setConnectTimeout(60000);
			connection.setReadTimeout(600000);
			connection.setRequestProperty("Accept-Charset", "utf-8");
			connection.setRequestProperty("contentType", "utf-8");
			connection.connect();

			OutputStream os = connection.getOutputStream();
			os.write(param);
			os.flush();
			os.close();

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
			int len = 0;
			byte[] buf = new byte[1024];
			while ((len = bis.read(buf)) != -1) {
				baos.write(buf, 0, len);
			}
			byte[] bufferByte = new byte[256];

			int l = -1;

			int downloadSize = 0;

			while ((l = bis.read(bufferByte)) > -1) {

				downloadSize += l;

				baos.write(bufferByte, 0, l);

				baos.flush();

			}
			String xml = baos.toString();
			File file2 = new File(localfilepath);
			FileWriter writer = new FileWriter(file2);
			writer.write(xml);
			writer.flush();
			writer.close();
			bis.close();
			connection.disconnect();
			return baos.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * {@link HttpClientErrorException}  {@link PostConstruct} 方法
	 * @param param
	 * @param posturl
	 * @return
	 * @throws Exception
	 */
	public  static String doPost(byte[] param, String posturl)throws Exception{
		String  xml=null;
		URL url = new URL(posturl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setRequestMethod("POST");
		connection.setUseCaches(false);
		connection.setInstanceFollowRedirects(true);
		connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
		connection.setConnectTimeout(60000);
		connection.setReadTimeout(600000);
		connection.setRequestProperty("Accept-Charset", "utf-8");
		connection.setRequestProperty("contentType", "utf-8");
		connection.connect();

		OutputStream os = connection.getOutputStream();
		os.write(param);
		os.flush();
		os.close();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
//		int len = 0;
//		byte[] buf = new byte[1024];
//		while ((len = bis.read(buf)) != -1) {
//			baos.write(buf, 0, len);
//		}
		byte[] bufferByte = new byte[256];

		int l = -1;

		int downloadSize = 0;

		while ((l = bis.read(bufferByte)) > -1) {

			downloadSize += l;

			baos.write(bufferByte, 0, l);

			baos.flush();

		}
		xml=baos.toString();
		System.out.println("回执信息->"+xml);
		return xml;
	}
	
	public  static String postMethod(String  posturl,String  content,String sign) throws Exception{
		String  xml=null;
		URL url = new URL(posturl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setRequestMethod("POST");
		connection.setUseCaches(false);
		connection.setInstanceFollowRedirects(true);
		connection.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
		connection.setConnectTimeout(60000);
		connection.setReadTimeout(600000);
		connection.setRequestProperty("Accept-Charset", "utf-8");
		connection.setRequestProperty("contentType", "utf-8");
		connection.connect();
		OutputStream os = connection.getOutputStream();
		
		String  param="sign_type=MD5&notify_type=COSCO_STOCK_OUT_ORDER&input_charset=UTF-8&sign="+sign+"&content="+content;
		os.write(param.getBytes());
		os.flush();
		os.close();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
		int len = 0;
		byte[] buf = new byte[1024];
		while ((len = bis.read(buf)) != -1) {
			baos.write(buf, 0, len);
		}
		byte[] bufferByte = new byte[256];

		int l = -1;

		int downloadSize = 0;

		while ((l = bis.read(bufferByte)) > -1) {

			downloadSize += l;

			baos.write(bufferByte, 0, l);

			baos.flush();

		}
		xml=baos.toString();
	
		return xml;
		
		

		
	}

}
