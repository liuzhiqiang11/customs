package com.bangmai.util;
import java.util.Map;

import com.bangmai.beans.Order2Customer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * json操作工具类
 * @author dellpc
 * @param <T>
 *
 */
public class JsonUtil {
	
	/**
	 * json转成map
	 * @param json
	 * @return
	 */
//	public static Map<String , Object> json2Map(String json){
//		Gson gson=new Gson();
//		Map<String, Object> resultMap = gson.fromJson( json, new TypeToken<Map<String, Object>>() { }.getType());
//		return resultMap;
//	}
	/**
	 *json转成bean
	 * @param json
	 * @return
	 */
	public  static <T> Class<T> json2Bean(String json){
		Gson gson=new Gson();
		 Class<T> t=null;
		t=gson.fromJson(json, Object.class);
		
		return t;
		
	}
	
	

}
