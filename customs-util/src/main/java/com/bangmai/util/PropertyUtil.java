package com.bangmai.util;

/**
 * 属性文件读取
 * @author PC043
 *
 */
public class PropertyUtil {

	private Integer cacheTimeOut;//缓存过期时间
	private String hg_file_path;//海关上传报文存放路径
	private String sj_file_path;//商检上传报文存放路径
	private String rk_file_path;//入库上传报文存放路径
	private String ck_file_path;//出库上传报文存放路径

	public Integer getCacheTimeOut() {
		return cacheTimeOut;
	}

	public void setCacheTimeOut(Integer cacheTimeOut) {
		this.cacheTimeOut = cacheTimeOut;
	}

	public String getHg_file_path() {
		return hg_file_path;
	}

	public void setHg_file_path(String hg_file_path) {
		this.hg_file_path = hg_file_path;
	}

	public String getSj_file_path() {
		return sj_file_path;
	}

	public void setSj_file_path(String sj_file_path) {
		this.sj_file_path = sj_file_path;
	}

	public String getRk_file_path() {
		return rk_file_path;
	}

	public void setRk_file_path(String rk_file_path) {
		this.rk_file_path = rk_file_path;
	}

	public String getCk_file_path() {
		return ck_file_path;
	}

	public void setCk_file_path(String ck_file_path) {
		this.ck_file_path = ck_file_path;
	}
	
	
}
