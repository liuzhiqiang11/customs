package com.bangmai.util;


import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;




/**
 * 任务执行HANDLE
 * @author PC043
 *
 */
public class QuartzJobFactory implements Job {
	
	Logger logger = LoggerFactory.getLogger(getClass());
	@Override
	public void execute(JobExecutionContext con) throws JobExecutionException {
		String name =con.getJobDetail().getJobDataMap().getString("name");
		logger.debug("任务"+name+"正在执行....");
		String jobContent = con.getJobDetail().getJobDataMap().getString("content");
		logger.debug("任务内容"+jobContent+"正在执行....");
		String result= HttpClientUtil.getInstance().sendHttpPost(jobContent);
		System.err.println(result);
		
	}

}
