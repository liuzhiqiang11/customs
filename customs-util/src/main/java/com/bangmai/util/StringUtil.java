package com.bangmai.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringUtil {
	// 把字符串转成list
	public static List<String> string2List(String string) {
		List<String> list = new ArrayList<>();
		if (string == null || string.isEmpty()) {
			return null;
		} else {
			String[] strings = string.trim().split("-");
			for (String string2 : strings) {
				System.err.println(string2);
			}

			list = Arrays.asList(strings);
		}
		return list;
	}

}
