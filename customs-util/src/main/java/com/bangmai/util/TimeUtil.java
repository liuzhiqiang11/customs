package com.bangmai.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimeUtil {
	
	/** 
	 * 获取指定时间对应的毫秒数 
	 * @param time "HH:mm:ss" 
	 * @return 
	 */  
	private static long getTimeMillis(String time) {  
	    try {  
	        DateFormat dateFormat = new SimpleDateFormat("yy-MM-dd HH:mm:ss");  
	        DateFormat dayFormat = new SimpleDateFormat("yy-MM-dd");  
	        Date curDate = dateFormat.parse(dayFormat.format(new Date()) + " " + time);  
	        return curDate.getTime();  
	    } catch (ParseException e) {  
	        e.printStackTrace();  
	    }  
	    return 0;  
	}  
	
	/**
	 * ȡ�õ�ǰʱ�� ��ʽΪ(yyyyMMddHHmmss)
	 * @return
	 */
	public static String getTimeString1(){
		
		Date nowTime=new Date(); 
		System.out.println(nowTime); 
		SimpleDateFormat time=new SimpleDateFormat("yyyyMMddHHmmss"); 
		return time.format(nowTime);
	}
	/**
	 * ȡ�õ�ǰʱ�� ��ʽΪ(yyyyMMddHHmmssms)
	 * @return
	 */
	public static String getTimeString2(){
		
		Date nowTime=new Date(); 
		System.out.println(nowTime); 
		SimpleDateFormat time=new SimpleDateFormat("yyyyMMddHHmmssms"); 
		return time.format(nowTime);
	}
	
	
	 public static Date string2Date(String dateStr) throws ParseException{
		 
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		 Date date = sdf.parse(dateStr);
		 return date;
		 
	 }	
	 
     public static Date string2Date2(String dateStr) throws ParseException{
		 
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		 Date date = sdf.parse(dateStr);
		 return date;
		 
	 }	
	
	public static String Date2String(Date date){
		   
		SimpleDateFormat time=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		return time.format(date);
		
	}
	
	public  static String  long2Date1(Long l){
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//		Calendar calendar = Calendar.getInstance();
//		calendar.setTimeInMillis(l);
		Date date=new Date(l);
		return formatter.format(date);
	}
	public  static String  long2Date2(Long l){
		DateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(l);
		return formatter.format(calendar.getTime());
	}
	
	
	public  static  String diyDate(String s) throws ParseException{
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMddhhmmss");
		Date date = (Date) sdf1.parse(s);
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sdf2.format(date);
		
	}
	
	public  static String diyDate1(String s) throws ParseException{
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = (Date) sdf1.parse(s);
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		return sdf2.format(date);
	}
	

}
