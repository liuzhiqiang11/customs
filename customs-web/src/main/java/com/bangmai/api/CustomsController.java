package com.bangmai.api;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bangmai.constant.Header;
import com.bangmai.exception.ServiceException;
import com.bangmai.model.MqMessage;
import com.bangmai.service.AnalysisService;
import com.bangmai.service.CustomsService;
import com.bangmai.view.MessageBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RequestMapping("/customs")
@Controller
public class CustomsController {
	Logger logger = Logger.getLogger(CustomsController.class);
	@Resource
	private CustomsService customsService;

	@Autowired
	private AnalysisService analysisService;
	@Autowired
	private AmqpTemplate amqpTemplate;

	/**
	 * 手动报关接口
	 * 
	 * @param type
	 *            类型
	 * @param order_id
	 *            订单号
	 * @return
	 */
	@RequestMapping(value = "createAndSendXml", method = RequestMethod.GET)
	@ResponseBody
	public MessageBean createAndSendXml(String types, String order_id) {
		MessageBean messageBea = new MessageBean();
		try {
			customsService.stockIn(types, order_id);
			messageBea.setMsg("报文创建并发送成功");
			messageBea.setCode(Header.STATUS_SUCESS);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			messageBea.setMsg(e.getMessage());
			messageBea.setCode(Header.STATUS_FATAL_ERROR);
		} catch (Exception e) {
			// messageBea.setMsg(e.getMessage());
			// messageBea.setCode(Header.STATUS_FATAL_ERROR);
			e.printStackTrace();
		}
		return messageBea;
	}

	/**
	 * 手动获得回执报文的接口
	 * 
	 * @param type
	 *            1-海关 2-商检 4-入库 5-出库
	 * @return
	 */
	@RequestMapping(value = "receivePacket", method = RequestMethod.GET)
	@ResponseBody
	public MessageBean receivePacket(int type) {
		MessageBean messageBean = new MessageBean();
		try {
			customsService.receivePacket(type);
			messageBean.setCode(Header.STATUS_SUCESS);
			messageBean.setMsg("下载成功");
		} catch (ServiceException e) {
			messageBean.setCode(Header.STATUS_FAILED);
			messageBean.setMsg(e.getMessage());
			// e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			messageBean.setCode(Header.STATUS_FATAL_ERROR);
			messageBean.setMsg(e.getMessage());
			// e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			messageBean.setCode(Header.STATUS_FATAL_ERROR);
			messageBean.setMsg(e.getMessage());
			// e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			messageBean.setCode(Header.STATUS_FATAL_ERROR);
			messageBean.setMsg(e.getMessage());
			// e.printStackTrace();
		}
		return messageBean;
	}

	/**
	 * 定时任务接口（获取回执）
	 * 
	 * @return
	 */

	@RequestMapping(method = RequestMethod.GET, value = "getReceiverXml")
	@ResponseBody
	public MessageBean getReceiverXml() {
		MessageBean messageBean = new MessageBean();
		try {
			customsService.getReceiverXml();
			messageBean.setCode(Header.STATUS_SUCESS);
			messageBean.setMsg("操作成功！！！");
		} catch (Exception e) {
			messageBean.setCode(Header.STATUS_FATAL_ERROR);
			messageBean.setMsg(e.getMessage());
		}
		return messageBean;

	}

	/**
	 * 通联支付异步通知地址
	 * 
	 * @param request
	 * @param response
	 */
	@ResponseBody
	@RequestMapping(value = "getPaymentPacket", method = RequestMethod.POST)
	public void getPaymentPacket(HttpServletRequest request, HttpServletResponse response) {
		MessageBean messageBean = new MessageBean();
		try {
			customsService.getPaymentNotifyXml(request, response);
			messageBean.setCode(Header.STATUS_SUCESS);
			messageBean.setMsg("操作成功");
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			messageBean.setCode(Header.STATUS_FATAL_ERROR);
			messageBean.setMsg(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			messageBean.setCode(Header.STATUS_FATAL_ERROR);
			messageBean.setMsg(e.getMessage());
		}

	}

	/**
	 * 入库
	 * 
	 * @param skuCode
	 *            sku编码
	 * @param logistics_code
	 *            物流状态
	 * @param qty
	 *            入库的数量
	 * @return
	 */

	@ResponseBody
	@RequestMapping(value = "immigration", method = RequestMethod.GET)
	public MessageBean immigration(String skuCode, String logistics_code, String qty) {
		MessageBean messageBean = new MessageBean();
		try {

			customsService.updateGood(skuCode, logistics_code, qty);
			messageBean.setCode(Header.STATUS_SUCESS);
			messageBean.setMsg("操作成功");
		} catch (ServiceException e) {
			messageBean.setCode(Header.STATUS_FATAL_ERROR);
			messageBean.setMsg(e.getMessage());
			// e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			messageBean.setCode(Header.STATUS_FATAL_ERROR);
			messageBean.setMsg(e.getMessage());
			// e.printStackTrace();
		}
		return messageBean;
	}

	/**
	 * 出库接口
	 * 
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/stockOut", method = RequestMethod.GET)
	private MessageBean stockOut() {
		MessageBean messageBean = new MessageBean();
		try {
			customsService.stockOut();
			messageBean.setCode(Header.STATUS_SUCESS);
			messageBean.setMsg("操作成功");
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			messageBean.setCode(Header.STATUS_FAILED);
			messageBean.setMsg(e.getMessage());
		} catch (Exception e) {
			messageBean.setCode(Header.STATUS_FATAL_ERROR);
			messageBean.setMsg(e.getMessage());
		}
		return messageBean;
	}

	/**
	 * 出库成功后 向网站发送信息
	 * 
	 * @return
	 */
	@RequestMapping(value = "/sendMsg2Web", method = RequestMethod.GET)
	@ResponseBody
	public MessageBean sendMsg2Web(String order_id) {
		
		MessageBean messageBean = new MessageBean();
		MqMessage mqMessage = new MqMessage();
		mqMessage.setDatetime("145987788899");
		mqMessage.setLogistics_code("74558411");
		mqMessage.setLogistics_name("申通快递");
		mqMessage.setOrderId("74568923654");
		mqMessage.setRemarke("暂无备注");
		try {
			customsService.sendMqmessage2Web(mqMessage, order_id);
			messageBean.setCode(Header.STATUS_SUCESS);
			messageBean.setMsg("发送成功！！！");
			messageBean.setBody(mqMessage);
		} catch (ServiceException e) {
			messageBean.setCode(Header.STATUS_FAILED);
			messageBean.setMsg(e.getMessage());
		}catch (Exception e) {
			// TODO Auto-generated catch block
			messageBean.setCode(Header.STATUS_FATAL_ERROR);
			messageBean.setMsg(e.getMessage());
		}

		
		return messageBean;
	}

}
