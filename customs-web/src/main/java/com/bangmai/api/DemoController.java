package com.bangmai.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import javax.annotation.Resource;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bangmai.beans.Order2Immigration;
import com.bangmai.beans.OrderGood2QiBang;
import com.bangmai.beans.QiBangOrderItemList;
import com.bangmai.beans.QiBangRoot;
import com.bangmai.constant.Header;

import com.bangmai.model.Demo;
import com.bangmai.model.Good;
import com.bangmai.model.GoodMapper;
import com.bangmai.model.MqGood;
import com.bangmai.model.MqOrder;
import com.bangmai.model.Order;
import com.bangmai.model.OrderStatus;
import com.bangmai.model.OrderStatusMapper;
import com.bangmai.service.CreateBeanService;
import com.bangmai.service.CustomsService;
import com.bangmai.service.DemoService;
import com.bangmai.util.ExecutorPool;
import com.bangmai.util.JsonUtil;
import com.bangmai.util.TimeUtil;
import com.bangmai.util.XmlUtil;
import com.bangmai.view.MessageBean;

@RequestMapping("/demo")
@Controller
public class DemoController {
	Logger logger = LoggerFactory.getLogger(getClass());
	@Resource
	private DemoService demoService;
	@Autowired
	private XmlUtil xmlUtil;
	@Autowired
	private CreateBeanService createBeanService;
	@Autowired
	private OrderStatusMapper orderStatusMapper;
	@Autowired
	@Qualifier("pool")
	private ExecutorPool pool;
	@Autowired
	private CustomsService customsService;
	@Autowired
	private GoodMapper goodMapper;
	@Autowired
	private AmqpTemplate amqpTemplate;

	@RequestMapping(value = "save", method = RequestMethod.GET)
	@ResponseBody
	public MessageBean save(Demo demo) {
		MessageBean msg = new MessageBean();
		try {
			// demoService.save(demo);

			msg.setCode(Header.STATUS_SUCESS);
			msg.setMsg("成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			msg.setCode(Header.STATUS_FATAL_ERROR);
			msg.setMsg(e.getMessage());
		}
		return msg;
	}

	@RequestMapping(value = "update", method = RequestMethod.GET)
	@ResponseBody
	public MessageBean update(Demo demo) {
		MessageBean msg = new MessageBean();
		try {
			demoService.update(demo);
			msg.setCode(Header.STATUS_SUCESS);
			msg.setMsg("成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			msg.setCode(Header.STATUS_FATAL_ERROR);
			msg.setMsg(e.getMessage());
		}
		return msg;
	}

	@RequestMapping(value = "createXml", method = RequestMethod.GET)
	@ResponseBody
	public QiBangRoot createXml() {

		QiBangRoot root = new QiBangRoot();

		QiBangOrderItemList list = new QiBangOrderItemList();
		ArrayList<OrderGood2QiBang> arrayList = new ArrayList<OrderGood2QiBang>();
		OrderGood2QiBang order_1 = new OrderGood2QiBang();
		order_1.setV_goods_regist_no("646685300");
		order_1.setOrder_item_id("9888889401480715");
		order_1.setItem_id("134230000003105017");
		order_1.setItem_name("花王牌纸尿裤大号L54");
		order_1.setItem_code("kk101004");
		order_1.setInventory_type(1);
		order_1.setItem_quantity("1");
		order_1.setItem_price("100.00");
		order_1.setItem_version("6");
		order_1.setAttributes("actualPrice:10300;");
		order_1.setCus_code("defaultStr");
		order_1.setSku_code("defaultStr");

		arrayList.add(order_1);
		list.setList(arrayList);
		root.setStore_code("WPH-00001");
		root.setOrder_code("WPH0014239392009");
		root.setOrder_type("001");
		root.setOrder_source("501");
		root.setOrder_create_time(TimeUtil.Date2String(new Date()));
		root.setV_ieflag("I");
		root.setV_consignor_code("441421199011116666");
		root.setV_card_type("01");
		root.setV_transport_code("1");
		root.setV_package_typecode("1");
		root.setV_qy_state("142");
		root.setV_master_good_name("花王牌纸尿裤大号L54");
		root.setN_kos("2");
		root.setV_traf_name("飞机");
		root.setTms_service_code("UC");
		root.setTms_order_code("");
		root.setPrev_order_code("");
		root.setTotalmailno("");
		root.setReceiver_info("^^^新疆维吾尔自治区^^^乌鲁木齐市^^^新市区^^^二工街道 测试地址1111^^^王二锤^^^15000120145^^^NA");
		root.setSender_info("123456^^^广东省^^^广州市^^^天河区^^^测试地址001^^^中远^^^NA^^^12345678");
		root.setPackage_count("2");
		root.setDelivery_method("飞机");
		root.setRemark("暂无备注");
		root.setOrder_ename("zhangshan");
		root.setOrder_phone("15269354895");
		root.setOrder_cardno("510811198811162395");
		root.setFreight("55.00");
		root.setTax("16.00");
		root.setInsurance_fee("25.00");
		root.setList(list);

		return root;
	}

	@RequestMapping(value = "/selectOrder", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> selectOrder(Integer order_id) {
		return demoService.select(order_id);

	}

	@RequestMapping(value = "/selectGoods", method = RequestMethod.GET)
	@ResponseBody
	public MessageBean selectGoods() {
		MessageBean messageBean=new MessageBean();
		List<String > skuCodes=new ArrayList<>();
		skuCodes.add("test0001");
		skuCodes.add("test0002");
		try {
			Map<String, Object> goods=demoService.selectGoods("111");
			messageBean.setBody(goods);
			messageBean.setCode(Header.STATUS_SUCESS);
			messageBean.setMsg("操作成功");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return messageBean;
	}

	


	
	@ResponseBody
	@RequestMapping(value="/selectProvinceCode",method=RequestMethod.GET)
	public  MessageBean selectProvinceCode(){
		MessageBean messageBean=new MessageBean();
		try {
			List<String> strings=goodMapper.selectProvinceCode();
			messageBean.setCode(Header.STATUS_SUCESS);
			messageBean.setBody(strings);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		
		return messageBean;
	}
	
//	@ResponseBody
//	@RequestMapping(value = "/send", method = RequestMethod.GET)
//	public MessageBean send(String orderNo) {
//		MessageBean messageBean = new MessageBean();
//		ObjectMapper mapper = new ObjectMapper();
//		 String json =null;
//		 try {
//			 MqOrder order = new MqOrder();
//			 order.setIdType("01");
//			 order.setInsuranceFee("11");
//			 order.setInvoiceCon("猛追湾一哥的单");
//			 order.setInvoiceTitle("liu");
//			 order.setIsNeedInvoice("0");
//			 order.setIsNeedList("1");
//			 order.setOrderDate(String.valueOf(new Date().getTime()));
//			 order.setOrderNo(orderNo);
//			 order.setPayDate(String.valueOf(new Date().getTime()));
//			 order.setPayNo("33525");
//			 order.setPayWay("01");
//			 order.setRecipientAddr("猛追湾");
//			 order.setRecipientAddrDetail("四川省成都市");
//			 order.setRecipientTel("18628058993");
//			 order.setShippingCost("3424");
//			 order.setShippingMonetary("11");
//			 order.setTel("02887785332");
//			 order.setTotal("54");
//			 order.setTotalMonetary("2342");
//			 order.setUsrName("lq");
//			 order.setUsrNum("007");
//			 List<MqGood> mqGoods = new ArrayList<MqGood>();
//			 MqGood good = new MqGood();
//			 good.setAmount("3");
//			 good.setCargoNo("33");
//			 good.setOrderNo(order.getOrderNo());
//			 good.setPrice("555");
//			 good.setSkuCode("test0001");
//			 mqGoods.add(good);
//			 MqGood good1 = new MqGood();
//			 good1.setAmount("44");
//			 good1.setCargoNo("777");
//			 good1.setOrderNo(order.getOrderNo());
//			 good1.setPrice("66");
//			 good1.setSkuCode("test0002");
//			 mqGoods.add(good1);
//			 order.setMqGoods(mqGoods);
//			json = mapper.writeValueAsString(order);
//			amqpTemplate.convertAndSend("test_custom_order_queue_key",order);
//		} catch (IOException e) {
//			logger.error(e.getMessage());
//		} finally {
//			mapper = null;
//		}
//		messageBean.setBody(json);
//		return messageBean;
//	}
}
