package com.bangmai.listener;
/**
 * 海关
 */

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.bangmai.model.MqOrder;
import com.bangmai.service.CustomsService;
@Component
public class TaskCustom  implements Runnable{
	Logger logger=Logger.getLogger(TaskCustom.class);
	private CustomsService customsService;
	private String order_id;
	public TaskCustom(CustomsService customsService, String order_id) {
		super();
		this.customsService = customsService;
		this.order_id=order_id;
	}
	public TaskCustom() {
		super();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			logger.info("启动海关流程...");
			customsService.createAndSendPacket(1,order_id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
