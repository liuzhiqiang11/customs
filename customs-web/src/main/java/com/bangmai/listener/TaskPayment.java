package com.bangmai.listener;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.bangmai.model.MqOrder;
import com.bangmai.service.CustomsService;
@Component
public class TaskPayment  implements Runnable{
	Logger logger=Logger.getLogger(TaskPayment.class);
	private CustomsService customsService;
	private String order_id;
	public TaskPayment(CustomsService customsService, String order_id) {
		super();
		this.customsService = customsService;
		this.order_id=order_id;
	}
	

public TaskPayment() {
		super();
	}


@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			logger.info("启动通联支付流程...");
			customsService.createAndSendPacket(3, order_id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
